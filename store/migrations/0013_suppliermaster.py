# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20150324_0921'),
        ('store', '0012_countrymaster_statemaster'),
    ]

    operations = [
        migrations.CreateModel(
            name='SupplierMaster',
            fields=[
                ('supplier_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nSupplierID')),
                ('supplier_code', models.CharField(max_length=10, db_column=b'cSupplierCode')),
                ('supplier_name', models.CharField(max_length=50, db_column=b'cSupplierName')),
                ('contact_person', models.CharField(max_length=50, db_column=b'cContactPerson', blank=True)),
                ('contact_phone', models.CharField(max_length=25, db_column=b'cContactPhone', blank=True)),
                ('contact_mobile', models.CharField(max_length=25, db_column=b'cContactMobile', blank=True)),
                ('contact_email', models.CharField(max_length=50, db_column=b'cContactEmail', blank=True)),
                ('supplier_address', models.CharField(max_length=150, db_column=b'cSupplierAddress', blank=True)),
                ('pincode', models.CharField(max_length=15, db_column=b'cPincode', blank=True)),
                ('office_phone', models.CharField(max_length=20, db_column=b'cOfficePhone', blank=True)),
                ('office_mobile', models.CharField(max_length=20, db_column=b'cOfficeMobile', blank=True)),
                ('office_email_id', models.CharField(max_length=50, db_column=b'cOfficeEmailID', blank=True)),
                ('link_account_id', models.IntegerField(db_column=b'nLinkAccountID')),
                ('is_active', models.BooleanField(default=True, db_column=b'cIsActive')),
                ('is_billwisesettle', models.BooleanField(default=True, db_column=b'cIsBillWiseSettle')),
                ('credit_limit', models.DecimalField(decimal_places=2, max_digits=9, db_column=b'nCreditLimit')),
                ('is_blacklisted', models.BooleanField(default=True, db_column=b'cIsBlackListed')),
                ('supplier_bank', models.CharField(max_length=50, db_column=b'cSupplierBank', blank=True)),
                ('supplier_branch', models.CharField(max_length=50, db_column=b'cSupplierBranch', blank=True)),
                ('supplier_account_no', models.CharField(max_length=50, db_column=b'cSupplierAccountNo', blank=True)),
                ('supplier_ifsc', models.CharField(max_length=50, db_column=b'cSupplierIFSC', blank=True)),
                ('supplier_kgst_no', models.CharField(max_length=25, db_column=b'cSupplierKGSTNo', blank=True)),
                ('supplier_cst_no', models.CharField(max_length=25, db_column=b'cSupplierCSTNo', blank=True)),
                ('supplier_pan', models.CharField(max_length=25, db_column=b'cSupplierPan', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('branch_id', models.ForeignKey(db_column=b'nBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('country_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, db_column=b'nCountryID', blank=True, to='store.CountryMaster', null=True)),
                ('data_add_by', models.ForeignKey(related_name='supplier_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='supplier_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('state_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, db_column=b'nStateID', blank=True, to='store.StateMaster', null=True)),
            ],
            options={
                'db_table': 'tstsuppliermaster',
            },
            bases=(models.Model,),
        ),
    ]
