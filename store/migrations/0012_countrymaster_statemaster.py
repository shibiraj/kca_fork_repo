# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0011_itemmaster_item_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='CountryMaster',
            fields=[
                ('country_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nCountryId')),
                ('country_name', models.CharField(max_length=50, db_column=b'nCountryName')),
            ],
            options={
                'db_table': 'tstcountrymaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StateMaster',
            fields=[
                ('state_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nStateId')),
                ('state_name', models.CharField(max_length=50, db_column=b'nStateName')),
                ('country_id', models.ForeignKey(db_column=b'nCountryId', on_delete=django.db.models.deletion.PROTECT, to='store.CountryMaster')),
            ],
            options={
                'db_table': 'tststatemaster',
            },
            bases=(models.Model,),
        ),
    ]
