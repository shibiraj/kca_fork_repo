# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RackMaster',
            fields=[
                ('rack_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nRackID')),
                ('rack_location', models.CharField(max_length=50, db_column=b'cRackLocation')),
                ('remarks', models.CharField(max_length=300, db_column=b'cRemarks', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='rack_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='rack_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tstrackmaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StoreMaster',
            fields=[
                ('store_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nStoreID')),
                ('store_code', models.CharField(max_length=10, db_column=b'cStoreCode')),
                ('store_name', models.CharField(max_length=50, db_column=b'cStoreName')),
                ('remarks', models.CharField(max_length=300, db_column=b'cRemarks', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('branch_id', models.ForeignKey(db_column=b'nBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='store_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='store_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tststoremaster',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='rackmaster',
            name='store_id',
            field=models.ForeignKey(db_column=b'nStoreID', on_delete=django.db.models.deletion.PROTECT, to='store.StoreMaster'),
            preserve_default=True,
        ),
    ]
