# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0009_rfqhead_items'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rfqhead',
            name='action',
            field=models.IntegerField(null=True, db_column=b'cAction'),
            preserve_default=True,
        ),
    ]
