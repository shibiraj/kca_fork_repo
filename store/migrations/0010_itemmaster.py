# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20150324_0921'),
        ('store', '0009_auto_20150326_1533'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemMaster',
            fields=[
                ('item_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nItemID')),
                ('item_code', models.CharField(max_length=10, db_column=b'cItemCode')),
                ('item_short_code', models.CharField(max_length=20, db_column=b'cItemShortCode')),
                ('item_name', models.CharField(max_length=50, db_column=b'cItemName')),
                ('item_description', models.CharField(max_length=150, db_column=b'cItemDescription')),
                ('reorder_level', models.FloatField(default=0, db_column=b'cReOrderLevel')),
                ('is_active', models.BooleanField(default=True, db_column=b'cIsActive')),
                ('remarks', models.CharField(max_length=300, null=True, db_column=b'cRemarks')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('brand_id', models.ForeignKey(db_column=b'nBrandID', on_delete=django.db.models.deletion.PROTECT, to='store.BrandMaster', null=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='item_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='item_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('item_subgroup_id', models.ForeignKey(db_column=b'nItemSubGroupID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemSubGroup', null=True)),
                ('itemgroup_id', models.ForeignKey(db_column=b'nItemGroupID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemGroup')),
                ('pattern_id', models.ForeignKey(db_column=b'nPatternID', on_delete=django.db.models.deletion.PROTECT, to='store.PatternMaster', null=True)),
                ('purchaseaccount_id', models.ForeignKey(db_column=b'nPurchaseAccountID', on_delete=django.db.models.deletion.PROTECT, to='store.CorporateAccount')),
                ('size_id', models.ForeignKey(db_column=b'nSizeID', on_delete=django.db.models.deletion.PROTECT, to='store.SizeMaster', null=True)),
                ('unit_id', models.ForeignKey(db_column=b'nUnitID', on_delete=django.db.models.deletion.PROTECT, to='store.UnitMaster')),
            ],
            options={
                'db_table': 'tstitemmaster',
            },
            bases=(models.Model,),
        ),
    ]
