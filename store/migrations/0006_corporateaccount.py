# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
        ('store', '0005_patternmaster'),
    ]

    operations = [
        migrations.CreateModel(
            name='CorporateAccount',
            fields=[
                ('corporatehead_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nCorporateHeadID')),
                ('corporatehead_name', models.CharField(max_length=100, db_column=b'cCorporateHeadName')),
                ('parent_id', models.IntegerField(null=True, db_column=b'nParentId', blank=True)),
                ('account_flag', models.IntegerField(db_column=b'nAccountFlag')),
                ('level', models.IntegerField(db_column=b'nLevel')),
                ('allowmultiple_accounts', models.CharField(max_length=1, db_column=b'cAllowMultipleAccounts')),
                ('allbranches', models.CharField(max_length=1, db_column=b'cAllBranches')),
                ('depreciation_account', models.IntegerField(null=True, db_column=b'nDepreciationAccount', blank=True)),
                ('depfirsthalf_perc', models.FloatField(null=True, db_column=b'nDepFirstHalfPerc', blank=True)),
                ('depsecondhalf_perc', models.FloatField(null=True, db_column=b'nDepSecondHalfPerc', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='corporateaccount_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='corporateaccount_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tfacorporateaccount',
            },
            bases=(models.Model,),
        ),
    ]
