# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import store.models


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20150324_0921'),
        ('store', '0007_itemgroup'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemMaster',
            fields=[
                ('item_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nItemID')),
                ('item_code', models.CharField(max_length=10, db_column=b'cItemCode')),
                ('item_short_code', models.CharField(max_length=20, db_column=b'cItemShortCode')),
                ('item_name', models.CharField(max_length=50, db_column=b'cItemName')),
                ('item_description', models.CharField(max_length=150, db_column=b'cItemDescription')),
                ('reorder_level', models.FloatField(default=0, db_column=b'cReOrderLevel')),
                ('is_active', models.BooleanField(default=True, db_column=b'cIsActive')),
                ('remarks', models.CharField(max_length=300, null=True, db_column=b'cRemarks')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('brand_id', models.ForeignKey(db_column=b'nBrandID', on_delete=django.db.models.deletion.PROTECT, to='store.BrandMaster', null=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='item_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='item_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tstitemmaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemSubGroup',
            fields=[
                ('item_subgroup_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nItemSubGroupID')),
                ('item_subgroup_code', models.CharField(max_length=10, db_column=b'cItemSubGroupCode')),
                ('item_subgroup_name', models.CharField(max_length=50, db_column=b'cItemSubGroupName')),
                ('remarks', models.CharField(max_length=300, null=True, db_column=b'cRemarks')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='item_subgroup_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='item_subgroup_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('itemgroup_id', models.ForeignKey(to='store.ItemGroup', db_column=b'nItemGroupID')),
            ],
            options={
                'db_table': 'tstitemsubgroup',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RFQDetails',
            fields=[
                ('rfq_details_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nRFQDetailsID')),
                ('req_qty', models.FloatField(db_column=b'nReqQty')),
                ('item_id', models.ForeignKey(db_column=b'nItemID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemMaster')),
            ],
            options={
                'db_table': 'tstrfqdetails',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RFQDocuments',
            fields=[
                ('rfq_document_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nRFQDocumentID')),
                ('req_image_ref', models.ImageField(upload_to=store.models.get_rfq_path, db_column=b'cRFQImageRef')),
            ],
            options={
                'db_table': 'tSTRFQDocuments',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RFQHead',
            fields=[
                ('rfq_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nRFQID')),
                ('rfq_no', models.CharField(max_length=10, db_column=b'cRFQNO')),
                ('rfq_date', models.DateTimeField(db_column=b'dRFQDate')),
                ('rfq_last_date', models.DateTimeField(db_column=b'dRFQLastDate')),
                ('terms_condition', models.TextField(null=True, db_column=b'cTermsCondition')),
                ('action', models.BooleanField(default=None, db_column=b'cAction')),
                ('action_date', models.DateField(null=True, db_column=b'dActionDate')),
                ('action_comments', models.CharField(max_length=300, null=True, db_column=b'cActionComments')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac')),
                ('branch_id', models.ForeignKey(db_column=b'nBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='rfq_creater', db_column=b'nDataAddBy', on_delete=django.db.models.deletion.PROTECT, to='administration.UserMaster')),
                ('data_modi_by', models.ForeignKey(related_name='rfq_updater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', to='administration.UserMaster', null=True)),
                ('forwarded_to', models.ForeignKey(db_column=b'nForwardedTo', to='administration.UserMaster', null=True)),
            ],
            options={
                'db_table': 'tstrfqhead',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='rfqdocuments',
            name='rfq_id',
            field=models.ForeignKey(db_column=b'nRFQID', on_delete=django.db.models.deletion.PROTECT, to='store.RFQHead'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rfqdetails',
            name='rfq_id',
            field=models.ForeignKey(db_column=b'nRFQID', on_delete=django.db.models.deletion.PROTECT, to='store.RFQHead'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='item_subgroup_id',
            field=models.ForeignKey(db_column=b'nItemSubGroupID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemSubGroup', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='itemgroup_id',
            field=models.ForeignKey(db_column=b'nItemGroupID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemGroup'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='pattern_id',
            field=models.ForeignKey(db_column=b'nPatternID', on_delete=django.db.models.deletion.PROTECT, to='store.PatternMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='purchaseaccount_id',
            field=models.ForeignKey(db_column=b'nPurchaseAccountID', on_delete=django.db.models.deletion.PROTECT, to='store.CorporateAccount'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='size_id',
            field=models.ForeignKey(db_column=b'nSizeID', on_delete=django.db.models.deletion.PROTECT, to='store.SizeMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemmaster',
            name='unit_id',
            field=models.ForeignKey(db_column=b'nUnitID', on_delete=django.db.models.deletion.PROTECT, to='store.UnitMaster'),
            preserve_default=True,
        ),
    ]
