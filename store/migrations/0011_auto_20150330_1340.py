# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import store.models


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20150324_0921'),
        ('store', '0010_auto_20150330_1253'),
    ]

    operations = [
        migrations.CreateModel(
            name='CountryMaster',
            fields=[
                ('country_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nCountryId')),
                ('country_name', models.CharField(max_length=50, db_column=b'nCountryName')),
            ],
            options={
                'db_table': 'tstcountrymaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuotationHead',
            fields=[
                ('quotation_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nQuotationID')),
                ('quotation_no', models.CharField(max_length=10, db_column=b'cQuotationNO')),
                ('quotation_date', models.DateTimeField(db_column=b'dQuotationDate')),
                ('supplier_name', models.CharField(max_length=50, null=True, db_column=b'cSupplierName')),
                ('supplier_address', models.CharField(max_length=150, null=True, db_column=b'cSupplierAddress')),
                ('contact_person', models.CharField(max_length=50, null=True, db_column=b'cContactPerson')),
                ('contact_no', models.CharField(max_length=25, null=True, db_column=b'cContactNo')),
                ('contact_email', models.CharField(max_length=50, null=True, db_column=b'cContactEmail')),
                ('total_cost', models.DecimalField(decimal_places=2, max_digits=9, db_column=b'nTotalCost')),
                ('delivery_schedule', models.CharField(max_length=500, null=True, db_column=b'cDeliverySchedule')),
                ('delivery_image_ref', models.ImageField(null=True, upload_to=store.models.get_quotation_path, db_column=b'cDeliveryImageRef')),
                ('payment_terms', models.CharField(max_length=500, null=True, db_column=b'cPaymentTerms')),
                ('payment_image_ref', models.ImageField(null=True, upload_to=store.models.get_quotation_path, db_column=b'cPaymentImageRef')),
                ('warranty', models.CharField(max_length=500, null=True, db_column=b'cWarranty')),
                ('warranty_image_ref', models.ImageField(null=True, upload_to=store.models.get_quotation_path, db_column=b'cwarrantyImageRef')),
                ('terms_condition', models.CharField(max_length=500, null=True, db_column=b'cTermsCondition')),
                ('terms_image_ref', models.ImageField(null=True, upload_to=store.models.get_quotation_path, db_column=b'cTermsImageRef')),
                ('action', models.IntegerField(null=True, db_column=b'cAction')),
                ('action_date', models.DateTimeField(null=True, db_column=b'dActionDate')),
                ('action_comments', models.CharField(max_length=300, null=True, db_column=b'cActionComments')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac')),
                ('branch_id', models.ForeignKey(db_column=b'nBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='quotation_head_creater', db_column=b'nDataAddBy', on_delete=django.db.models.deletion.PROTECT, to='administration.UserMaster')),
                ('data_modi_by', models.ForeignKey(related_name='quotation_head_updater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', to='administration.UserMaster', null=True)),
                ('forwarded_to', models.ForeignKey(db_column=b'nForwardedTo', to='administration.UserMaster', null=True)),
                ('rfq_id', models.ForeignKey(db_column=b'nRFQID', on_delete=django.db.models.deletion.PROTECT, to='store.RFQHead')),
            ],
            options={
                'db_table': 'tstquotationhead',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StateMaster',
            fields=[
                ('state_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nStateId')),
                ('state_name', models.CharField(max_length=50, db_column=b'nStateName')),
                ('country_id', models.ForeignKey(db_column=b'nCountryId', on_delete=django.db.models.deletion.PROTECT, to='store.CountryMaster')),
            ],
            options={
                'db_table': 'tststatemaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SupplierMaster',
            fields=[
                ('supplier_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nSupplierID')),
                ('supplier_code', models.CharField(max_length=10, db_column=b'cSupplierCode')),
                ('supplier_name', models.CharField(max_length=50, db_column=b'cSupplierName')),
                ('contact_person', models.CharField(max_length=50, null=True, db_column=b'cContactPerson')),
                ('contact_phone', models.CharField(max_length=25, null=True, db_column=b'cContactPhone')),
                ('contact_mobile', models.CharField(max_length=25, null=True, db_column=b'cContactMobile')),
                ('contact_email', models.CharField(max_length=50, null=True, db_column=b'cContactEmail')),
                ('supplier_address', models.CharField(max_length=150, null=True, db_column=b'cSupplierAddress')),
                ('pincode', models.CharField(max_length=15, null=True, db_column=b'cPincode')),
                ('office_phone', models.CharField(max_length=20, null=True, db_column=b'cOfficePhone')),
                ('office_mobile', models.CharField(max_length=20, null=True, db_column=b'cOfficeMobile')),
                ('office_email_id', models.CharField(max_length=50, null=True, db_column=b'cOfficeEmailID')),
                ('link_account_id', models.IntegerField(db_column=b'nLinkAccountID')),
                ('is_active', models.BooleanField(default=True, db_column=b'cIsActive')),
                ('is_billwisesettle', models.BooleanField(default=True, db_column=b'cIsBillWiseSettle')),
                ('credit_limit', models.DecimalField(decimal_places=2, max_digits=9, db_column=b'nCreditLimit')),
                ('is_blacklisted', models.BooleanField(default=True, db_column=b'cIsBlackListed')),
                ('supplier_bank', models.CharField(max_length=50, null=True, db_column=b'cSupplierBank')),
                ('supplier_branch', models.CharField(max_length=50, null=True, db_column=b'cSupplierBranch')),
                ('supplier_account_no', models.CharField(max_length=50, null=True, db_column=b'cSupplierAccountNo')),
                ('supplier_ifsc', models.CharField(max_length=50, null=True, db_column=b'cSupplierIFSC')),
                ('supplier_kgst_no', models.CharField(max_length=25, null=True, db_column=b'cSupplierKGSTNo')),
                ('supplier_cst_no', models.CharField(max_length=25, null=True, db_column=b'cSupplierCSTNo')),
                ('supplier_pan', models.CharField(max_length=25, null=True, db_column=b'cSupplierPan')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac')),
                ('branch_id', models.ForeignKey(db_column=b'nBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('country_id', models.ForeignKey(db_column=b'nCountryID', on_delete=django.db.models.deletion.PROTECT, to='store.CountryMaster', null=True)),
                ('data_add_by', models.ForeignKey(related_name='supplier_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='supplier_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', to='administration.UserMaster', null=True)),
                ('state_id', models.ForeignKey(db_column=b'nStateID', on_delete=django.db.models.deletion.PROTECT, to='store.StateMaster', null=True)),
            ],
            options={
                'db_table': 'tstsuppliermaster',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='quotationhead',
            name='supplier_id',
            field=models.ForeignKey(db_column=b'nSupplierID', to='store.SupplierMaster', null=True),
            preserve_default=True,
        ),
    ]
