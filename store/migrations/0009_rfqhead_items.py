# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0008_auto_20150325_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='rfqhead',
            name='items',
            field=models.ManyToManyField(to='store.ItemMaster', through='store.RFQDetails'),
            preserve_default=True,
        ),
    ]
