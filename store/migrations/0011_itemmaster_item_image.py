# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import store.models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0010_itemmaster'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemmaster',
            name='item_image',
            field=models.ImageField(null=True, upload_to=store.models.get_file_path, db_column=b'iItemImage'),
            preserve_default=True,
        ),
    ]
