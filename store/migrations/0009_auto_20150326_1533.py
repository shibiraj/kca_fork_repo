# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0008_itemsubgroup'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itemsubgroup',
            name='itemgroup_id',
            field=models.ForeignKey(db_column=b'nItemGroupID', on_delete=django.db.models.deletion.PROTECT, to='store.ItemGroup'),
            preserve_default=True,
        ),
    ]
