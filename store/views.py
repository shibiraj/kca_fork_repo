from django.shortcuts import render
from django.views.generic import View
# Create your views here.


class StoreView(View):
    template_name = 'store/add_store.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)


class RackView(View):
    template_name = 'store/add_rack.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class UnitView(View):
    template_name = 'store/add_unit.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class BrandView(View):
    template_name = 'store/add_brand.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class SizeView(View):
    template_name = 'store/add_size.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class PatternView(View):
    template_name = 'store/add_pattern.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)


class DashboardView(View):
    template_name = 'store/dashboard.html'

    def get(self, request):
        request.session['menu_id'] = menu_id
        context = {
        }
        return render(request, self.template_name, context)

class ItemGroupView(View):
    template_name = 'store/add_itemgroup.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class ItemSubGroupView(View):
    template_name = 'store/add_itemsubgroup.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class ItemView(View):
    template_name = 'store/add_item.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class SupplierView(View):
    template_name = 'store/add_supplier.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class CategoryView(View):
    template_name = 'store/add_category.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class ItemRackMappingView(View):
    template_name = 'store/item-rack_mapping.html'

    def get(self, request):
        context = {
        }
        return render(request, self.template_name, context)

class RFQView(View):
    template_name = 'store/rfq.html'

    def get(self, request):
        return render(request, self.template_name)
