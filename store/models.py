from django.db import models
from administration.models import CompanyMaster, BranchMaster, UserMaster


def get_rfq_path(instance, filename):
    """ returns the file location to store images """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/rfq', filename)


def get_quotation_path(instance, filename):
    """ returns the file location to store images """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/quotation', filename)

def get_file_path(instance, filename):
    """ returns the file location to store images """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/item', filename)


class StoreMaster(models.Model):

    """ Store master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    branch_id = models.ForeignKey(
        BranchMaster, db_column='nBranchID', on_delete=models.PROTECT)
    store_id = models.AutoField(db_column='nStoreID', primary_key=True)
    store_code = models.CharField(db_column='cStoreCode', max_length=10)
    store_name = models.CharField(db_column='cStoreName', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='store_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='store_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.company_id)

    class Meta:
        db_table = 'tststoremaster'


class RackMaster(models.Model):

    """ Rack master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    store_id = models.ForeignKey(
        StoreMaster, db_column='nStoreID', on_delete=models.PROTECT)
    rack_id = models.AutoField(db_column='nRackID', primary_key=True)
    rack_location = models.CharField(db_column='cRackLocation', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='rack_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='rack_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.rack_id)

    class Meta:
        db_table = 'tstrackmaster'


class UnitMaster(models.Model):

    """ Unit master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    unit_id = models.AutoField(db_column='nUnitID', primary_key=True)
    unit_name = models.CharField(db_column='cUnitName', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='unit_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='unit_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.unit_id)

    class Meta:
        db_table = 'tstunitmaster'


class BrandMaster(models.Model):

    """ Brand master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    brand_id = models.AutoField(db_column='nBrandID', primary_key=True)
    brand_code = models.CharField(db_column='cBrandCode', max_length=10)
    brand_name = models.CharField(db_column='cBrandName', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='brand_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='brand_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.brand_id)

    class Meta:
        db_table = 'tstbrandmaster'


class SizeMaster(models.Model):

    """ Size master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    size_id = models.AutoField(db_column='nSizeID', primary_key=True)
    size_name = models.CharField(db_column='cSizeName', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='size_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='size_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.size_id)

    class Meta:
        db_table = 'tstsizemaster'


class PatternMaster(models.Model):

    """ Pattern master table model """

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    pattern_id = models.AutoField(db_column='nPatterID', primary_key=True)
    pattern_name = models.CharField(db_column='cPatternName', max_length=50)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='pattern_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='pattern_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.pattern_id)

    class Meta:
        db_table = 'tstpatternmaster'


class CorporateAccount(models.Model):
    corporatehead_id = models.AutoField(
        db_column='nCorporateHeadID', primary_key=True)
    corporatehead_name = models.CharField(
        db_column='cCorporateHeadName', max_length=100)
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    parent_id = models.IntegerField(
        db_column='nParentId', blank=True, null=True)
    account_flag = models.IntegerField(db_column='nAccountFlag')
    level = models.IntegerField(db_column='nLevel')
    allowmultiple_accounts = models.CharField(
        db_column='cAllowMultipleAccounts', max_length=1)
    allbranches = models.CharField(db_column='cAllBranches', max_length=1)
    depreciation_account = models.IntegerField(
        db_column='nDepreciationAccount', blank=True, null=True)
    depfirsthalf_perc = models.FloatField(
        db_column='nDepFirstHalfPerc', blank=True, null=True)
    depsecondhalf_perc = models.FloatField(
        db_column='nDepSecondHalfPerc', blank=True, null=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='corporateaccount_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='corporateaccount_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.corporatehead_id)

    class Meta:
        db_table = 'tfacorporateaccount'


class ItemGroup(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    itemgroup_id = models.AutoField(db_column='nItemGroupID', primary_key=True)
    itemgroup_code = models.CharField(
        db_column='cItemGroupCode', max_length=10)
    itemgroup_name = models.CharField(
        db_column='cItemGroupName', max_length=50)
    purchaseaccount_id = models.ForeignKey(
        CorporateAccount, db_column='nPurchaseAccountID')
    remarks = models.CharField(db_column='cRemarks', max_length=300)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='itemgroup_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='itemgroup_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.itemgroup_id)

    class Meta:
        db_table = 'tstitemgroup'


class ItemSubGroup(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    item_subgroup_id = models.AutoField(
        db_column='nItemSubGroupID', primary_key=True)
    item_subgroup_code = models.CharField(
        db_column='cItemSubGroupCode', max_length=10)
    item_subgroup_name = models.CharField(
        db_column='cItemSubGroupName', max_length=50)
    itemgroup_id = models.ForeignKey(ItemGroup, db_column='nItemGroupID', on_delete=models.PROTECT)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, null=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='item_subgroup_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='item_subgroup_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.item_subgroup_id)

    class Meta:
        db_table = 'tstitemsubgroup'

class RFQHead(models.Model):

    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    branch_id = models.ForeignKey(
        BranchMaster, db_column='nBranchID', on_delete=models.PROTECT)
    rfq_id = models.AutoField(db_column='nRFQID', primary_key=True)
    rfq_no = models.IntegerField(db_column='cRFQNO')
    rfq_date = models.DateTimeField(db_column='dRFQDate')
    rfq_last_date = models.DateTimeField(db_column='dRFQLastDate')
    terms_condition = models.TextField(
        db_column='cTermsCondition',  null=True)
    forwarded_to = models.ForeignKey(
        UserMaster, db_column='nForwardedTo', null=True)
    action = models.IntegerField(db_column='cAction', null=True)
    action_date = models.DateField(
        db_column='dActionDate', null=True)
    action_comments = models.CharField(
        db_column='cActionComments', max_length=300, null=True)
    data_add_by = models.ForeignKey(
        UserMaster, db_column='nDataAddBy', related_name='rfq_creater', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(
        UserMaster, db_column='nDataModiBy', null=True, related_name='rfq_updater', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, null=True)
    items = models.ManyToManyField('ItemMaster', through='RFQDetails')

    class Meta:
        db_table = 'tstrfqhead'


class RFQDetails(models.Model):
    rfq_details_id = models.AutoField(
        db_column='nRFQDetailsID', primary_key=True)
    rfq_id = models.ForeignKey(
        RFQHead, db_column='nRFQID', on_delete=models.PROTECT)
    item_id = models.ForeignKey(
        'ItemMaster', db_column='nItemID', on_delete=models.PROTECT)
    req_qty = models.FloatField(db_column='nReqQty')

    class Meta:
        db_table = 'tstrfqdetails'


class RFQDocuments(models.Model):
    rfq_id = models.ForeignKey(
        RFQHead, db_column='nRFQID', on_delete=models.PROTECT)
    rfq_document_id = models.AutoField(
        db_column='nRFQDocumentID', primary_key=True)
    req_image_ref = models.ImageField(
        db_column='cRFQImageRef', upload_to=get_rfq_path)

    class Meta:
        db_table = 'tSTRFQDocuments'


class ItemMaster(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    item_id = models.AutoField(db_column='nItemID', primary_key=True)
    item_code = models.CharField(db_column='cItemCode', max_length=10)
    item_short_code = models.CharField(
        db_column='cItemShortCode', max_length=20)
    item_name = models.CharField(db_column='cItemName', max_length=50)
    item_description = models.CharField(
        db_column='cItemDescription', max_length=150)
    brand_id = models.ForeignKey(
        BrandMaster, db_column='nBrandID', on_delete=models.PROTECT, null=True)
    size_id = models.ForeignKey(
        SizeMaster, db_column='nSizeID', on_delete=models.PROTECT, null=True)
    unit_id = models.ForeignKey(
        UnitMaster, db_column='nUnitID', on_delete=models.PROTECT)
    pattern_id = models.ForeignKey(
        PatternMaster, db_column='nPatternID', on_delete=models.PROTECT, null=True)
    itemgroup_id = models.ForeignKey(
        ItemGroup, db_column='nItemGroupID', on_delete=models.PROTECT)
    item_subgroup_id = models.ForeignKey(
        ItemSubGroup, db_column='nItemSubGroupID', null=True, on_delete=models.PROTECT)
    reorder_level = models.FloatField(db_column='cReOrderLevel', default=0)
    is_active = models.BooleanField(db_column='cIsActive', default=True)
    purchaseaccount_id = models.ForeignKey(
        CorporateAccount, db_column='nPurchaseAccountID', on_delete=models.PROTECT)
    remarks = models.CharField(
        db_column='cRemarks', max_length=300, null=True)
    item_image = models.ImageField(
        db_column='iItemImage', upload_to=get_file_path, null=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='item_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='item_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.item_id)

    class Meta:
        db_table = 'tstitemmaster'


class CountryMaster(models.Model):

    country_id = models.AutoField(db_column='nCountryId', primary_key=True)
    country_name = models.CharField(db_column='nCountryName', max_length=50)

    def __unicode__(self):
        return unicode(self.country_id)

    class Meta:
        db_table = 'tstcountrymaster'

class StateMaster(models.Model):

    state_id = models.AutoField(db_column='nStateId', primary_key=True)
    country_id = models.ForeignKey(
        CountryMaster, db_column='nCountryId', on_delete=models.PROTECT)
    state_name = models.CharField(db_column='nStateName', max_length=50)

    def __unicode__(self):
        return unicode(self.state_id)

    class Meta:
        db_table = 'tststatemaster'


class SupplierMaster(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    branch_id = models.ForeignKey(
        BranchMaster, db_column='nBranchID', on_delete=models.PROTECT)
    supplier_id = models.AutoField(db_column='nSupplierID', primary_key=True)  
    supplier_code = models.CharField(db_column='cSupplierCode', max_length=10)  
    supplier_name = models.CharField(db_column='cSupplierName', max_length=50) 
    contact_person = models.CharField(db_column='cContactPerson', max_length=50, blank=True)
    contact_phone = models.CharField(db_column='cContactPhone', max_length=25, blank=True)
    contact_mobile = models.CharField(db_column='cContactMobile', max_length=25, blank=True)
    contact_email = models.CharField(db_column='cContactEmail', max_length=50, blank=True)
    supplier_address = models.CharField(db_column='cSupplierAddress', max_length=150, blank=True)
    country_id = models.ForeignKey(CountryMaster,db_column='nCountryID', blank=True, null=True, on_delete=models.PROTECT)
    state_id = models.ForeignKey(StateMaster,db_column='nStateID', blank=True, null=True,on_delete=models.PROTECT)
    pincode = models.CharField(db_column='cPincode', max_length=15, blank=True)
    office_phone = models.CharField(db_column='cOfficePhone', max_length=20, blank=True)
    office_mobile = models.CharField(db_column='cOfficeMobile', max_length=20, blank=True)
    office_email_id = models.CharField(db_column='cOfficeEmailID', max_length=50, blank=True) 
    link_account_id = models.IntegerField(db_column='nLinkAccountID')
    is_active = models.BooleanField(db_column='cIsActive', default=True)
    is_billwisesettle = models.BooleanField(db_column='cIsBillWiseSettle', default=True)
    credit_limit = models.DecimalField(db_column='nCreditLimit', max_digits=9, decimal_places=2)
    is_blacklisted = models.BooleanField(db_column='cIsBlackListed', default=True)
    supplier_bank = models.CharField(db_column='cSupplierBank', max_length=50, blank=True)
    supplier_branch = models.CharField(db_column='cSupplierBranch', max_length=50, blank=True)
    supplier_account_no = models.CharField(db_column='cSupplierAccountNo', max_length=50, blank=True)
    supplier_ifsc = models.CharField(db_column='cSupplierIFSC', max_length=50, blank=True)
    supplier_kgst_no = models.CharField(db_column='cSupplierKGSTNo', max_length=25, blank=True)
    supplier_cst_no = models.CharField(db_column='cSupplierCSTNo', max_length=25, blank=True)
    supplier_pan = models.CharField(db_column='cSupplierPan', max_length=25, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='supplier_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                    db_column='nDataModiBy', blank=True, null=True, related_name='supplier_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.supplier_id)

    class Meta:
        db_table = 'tstsuppliermaster'

class Category(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    category_id = models.AutoField(db_column='nCategoryID', primary_key=True) 
    category_code = models.CharField(db_column='cCategoryCode', max_length=10) 
    category_name = models.CharField(db_column='cCategoryName', max_length=50)
    remarks = models.CharField(db_column='cRemarks', max_length=300, blank=True)
    data_add_by = models.ForeignKey(UserMaster,
                                    db_column='nDataAddBy', blank=True, null=True, related_name='category_creator', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(UserMaster,
                                     db_column='nDataModiBy', blank=True, null=True, related_name='category_modifier', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, blank=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, blank=True, null=True)
    
    def __unicode__(self):
        return unicode(self.category_id)
        
    class Meta:
        db_table = 'tstcategory'

class ItemRackMapping(models.Model):
    store_id = models.ForeignKey(
        StoreMaster, db_column='nStoreID', on_delete=models.PROTECT)
    rack_id = models.ForeignKey(
        RackMaster, db_column='nRackID', on_delete=models.PROTECT)
    item_id = models.ForeignKey(
        ItemMaster, db_column='nItemID', on_delete=models.PROTECT)
    
    class Meta:
        db_table = 'tstitemrackmapping'


class QuotationHead(models.Model):
    company_id = models.ForeignKey(
        CompanyMaster, db_column='nCompanyID', on_delete=models.PROTECT)
    branch_id = models.ForeignKey(
        BranchMaster, db_column='nBranchID', on_delete=models.PROTECT)
    rfq_id = models.ForeignKey(
        RFQHead, db_column='nRFQID', on_delete=models.PROTECT)
    quotation_id = models.AutoField(
        db_column='nQuotationID', primary_key=True)
    quotation_no = models.CharField(db_column='cQuotationNO', max_length=10)
    quotation_date = models.DateTimeField(db_column='dQuotationDate')
    supplier_id = models.ForeignKey(
        SupplierMaster, db_column='nSupplierID', null=True)
    supplier_name = models.CharField(
        db_column='cSupplierName', max_length=50, null=True)
    supplier_address = models.CharField(
        db_column='cSupplierAddress', max_length=150, null=True)
    contact_person = models.CharField(
        db_column='cContactPerson', max_length=50, null=True)
    contact_no = models.CharField(
        db_column='cContactNo', max_length=25, null=True)
    contact_email = models.CharField(
        db_column='cContactEmail', max_length=50, null=True)
    total_cost = models.DecimalField(
        db_column='nTotalCost', max_digits=9, decimal_places=2)
    delivery_schedule = models.CharField(
        db_column='cDeliverySchedule', max_length=500, null=True)
    delivery_image_ref = models.ImageField(
        db_column='cDeliveryImageRef', upload_to=get_quotation_path, null=True)
    payment_terms = models.CharField(
        db_column='cPaymentTerms', max_length=500, null=True)
    payment_image_ref = models.ImageField(
        db_column='cPaymentImageRef',  upload_to=get_quotation_path, null=True)
    warranty = models.CharField(
        db_column='cWarranty', max_length=500, null=True)
    warranty_image_ref = models.ImageField(
        db_column='cwarrantyImageRef',  upload_to=get_quotation_path, null=True)
    terms_condition = models.CharField(
        db_column='cTermsCondition', max_length=500, null=True)
    terms_image_ref = models.ImageField(
        db_column='cTermsImageRef', upload_to=get_quotation_path, null=True)
    forwarded_to = models.ForeignKey(
        UserMaster, db_column='nForwardedTo', null=True)
    action = models.IntegerField(db_column='cAction', null=True)
    action_date = models.DateTimeField(
        db_column='dActionDate', null=True)
    action_comments = models.CharField(
        db_column='cActionComments', max_length=300, null=True)
    data_add_by = models.ForeignKey(
        UserMaster, db_column='nDataAddBy', related_name='quotation_head_creater', on_delete=models.PROTECT)
    data_add_on = models.DateTimeField(
        db_column='dDataAddOn', auto_now_add=True)
    data_add_mac = models.CharField(db_column='cDataAddMac', max_length=50)
    data_modi_by = models.ForeignKey(
        UserMaster, db_column='nDataModiBy', null=True, related_name='quotation_head_updater', on_delete=models.PROTECT)
    data_modi_on = models.DateTimeField(
        db_column='dDataModiOn', auto_now=True, null=True)
    data_modi_mac = models.CharField(
        db_column='cDataModiMac', max_length=50, null=True)

    class Meta:
        db_table = 'tstquotationhead'
