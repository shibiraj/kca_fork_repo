Django==1.7.4
MySQL-python==1.2.5
Pillow==2.5.3
rest_framework==3.1.0
http://wadofstuff.googlecode.com/files/wadofstuff-django-serializers-1.1.0.tar.gz
simplejson=2.0.9
enum