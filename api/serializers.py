from rest_framework import serializers, viewsets
from django.contrib.auth.models import User
from administration.models import FinancialYear, CompanyMaster
# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

class CompanyMasterSerializer(serializers.ModelSerializer):
	class Meta:
		model = CompanyMaster
		fields = ('company_id','company_name','company_addressline1','company_addressline2')

class FinancialYearSerializer(serializers.ModelSerializer):
	company_id = serializers.PrimaryKeyRelatedField(queryset=CompanyMaster.objects.all())
	# company_ref = CompanyMasterSerializer(source='company_id')
	class Meta:
		model = FinancialYear
		fields = ('company_id','fin_year_code','starts_from','starts_to')