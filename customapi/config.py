import re
from administration.api import FinancialyearAPI, BranchMasterAPI, ModuleMasterAPI, UserMasterAPI, UserPermissionsAPI
from store.api import StoreMasterAPI, RackMasterAPI, UnitMasterAPI, BrandMasterAPI, SizeMasterAPI, PatternMasterAPI, CorporateAccountAPI, ItemGroupAPI, ItemSubGroupAPI, ItemMasterAPI, SupplierMasterAPI, CountryMasterAPI, StateMasterAPI, CategoryAPI, ItemRackMappingAPI, RFQMasterAPI
from kca.common.api import ModuleAPI, PasswordAPI


class APISelector(object):
    class_selectors = {
        'financialyears$': {
            'GET': [FinancialyearAPI, 'get_all'],
            'POST': [FinancialyearAPI, 'save_financialyear'],
            'status': True,
        },
        'financialyear/([\d]+)$': {
            'GET': [FinancialyearAPI, 'get_financialyear'],
            'DELETE': [FinancialyearAPI, 'delete_one'],
            'POST': [FinancialyearAPI, 'edit_financialyear'],
            'status': True
        },
        'branches$': {
            'GET': [BranchMasterAPI, 'get_all'],
            'POST': [BranchMasterAPI, 'save_branch'],
            'status': True,
        },
        'branch/([\d]+)$': {
            'GET': [BranchMasterAPI, 'get_branch'],
            'DELETE': [BranchMasterAPI, 'delete_one'],
            'POST': [BranchMasterAPI, 'edit_branch'],
            'status': True
        },
        'modules$': {
            'GET': [ModuleMasterAPI, 'get_all'],
            'status': True,
        },
        'users$': {
            'GET': [UserMasterAPI, 'get_all'],
            'POST': [UserMasterAPI, 'save_user'],
            'status': True,
        },
        'activeusers$': {
            'GET': [UserMasterAPI, 'get_active_users'],
            'status': True,
        },
        'user/([\d]+)$': {
            'GET': [UserMasterAPI, 'get_user'],
            'DELETE': [UserMasterAPI, 'delete_one'],
            'POST': [UserMasterAPI, 'edit_user'],
            'status': True
        },
        'userpermission/([\d]+)$': {
            'GET': [UserPermissionsAPI, 'get_user_permission'],
            'POST': [UserPermissionsAPI, 'save_user_permission'],
            'status': True
        },
        'stores$': {
            'GET': [StoreMasterAPI, 'get_all'],
            'POST': [StoreMasterAPI, 'save_store'],
            'status': True,
        },
        'store/([\d]+)$': {
            'GET': [StoreMasterAPI, 'get_store'],
            'DELETE': [StoreMasterAPI, 'delete_one'],
            'POST': [StoreMasterAPI, 'edit_store'],
            'status': True,
        },
        'racks$': {
            'GET': [RackMasterAPI, 'get_all'],
            'POST': [RackMasterAPI, 'save_rack'],
            'status': True,
        },
        'rack/([\d]+)$': {
            'GET': [RackMasterAPI, 'get_rack'],
            'DELETE': [RackMasterAPI, 'delete_one'],
            'POST': [RackMasterAPI, 'edit_rack'],
            'status': True,
        },
        'changepassword$': {
            'GET': [],
            'POST': [PasswordAPI, 'change_password'],
            'status': True,
        },
        'units$': {
            'GET': [UnitMasterAPI, 'get_all'],
            'POST': [UnitMasterAPI, 'save_unit'],
            'status': True,
        },
        'unit/([\d]+)$': {
            'GET': [UnitMasterAPI, 'get_unit'],
            'DELETE': [UnitMasterAPI, 'delete_one'],
            'POST': [UnitMasterAPI, 'edit_unit'],
            'status': True,
        },
        'brands$': {
            'GET': [BrandMasterAPI, 'get_all'],
            'POST': [BrandMasterAPI, 'save_brand'],
            'status': True,
        },
        'brand/([\d]+)$': {
            'GET': [BrandMasterAPI, 'get_brand'],
            'DELETE': [BrandMasterAPI, 'delete_one'],
            'POST': [BrandMasterAPI, 'edit_brand'],
            'status': True,
        },
        'sizes$': {
            'GET': [SizeMasterAPI, 'get_all'],
            'POST': [SizeMasterAPI, 'save_size'],
            'status': True,
        },
        'size/([\d]+)$': {
            'GET': [SizeMasterAPI, 'get_size'],
            'DELETE': [SizeMasterAPI, 'delete_one'],
            'POST': [SizeMasterAPI, 'edit_size'],
            'status': True,
        },
        'patterns$': {
            'GET': [PatternMasterAPI, 'get_all'],
            'POST': [PatternMasterAPI, 'save_pattern'],
            'status': True,
        },
        'pattern/([\d]+)$': {
            'GET': [PatternMasterAPI, 'get_pattern'],
            'DELETE': [PatternMasterAPI, 'delete_one'],
            'POST': [PatternMasterAPI, 'edit_pattern'],
            'status': True,
        },
        'corporateaccounts$': {
            'GET': [CorporateAccountAPI, 'get_all'],
            'status': True,
        },
        'itemgroups$': {
            'GET': [ItemGroupAPI, 'get_all'],
            'POST': [ItemGroupAPI, 'save_itemgroup'],
            'status': True,
        },
        'itemgroup/([\d]+)$': {
            'GET': [ItemGroupAPI, 'get_itemgroup'],
            'DELETE': [ItemGroupAPI, 'delete_one'],
            'POST': [ItemGroupAPI, 'edit_itemgroup'],
            'status': True,
        },
        'itemsubgroups$': {
            'GET': [ItemSubGroupAPI, 'get_all'],
            'POST': [ItemSubGroupAPI, 'save_itemsubgroup'],
            'status': True,
        },
        'itemsubgroup/([\d]+)$': {
            'GET': [ItemSubGroupAPI, 'get_itemsubgroup'],
            'DELETE': [ItemSubGroupAPI, 'delete_one'],
            'POST': [ItemSubGroupAPI, 'edit_itemsubgroup'],
            'status': True,
        },
        'items$': {
            'GET': [ItemMasterAPI, 'get_all'],
            'POST': [ItemMasterAPI, 'save_item'],
            'status': True,
        },
        'item/([\d]+)$': {
            'GET': [ItemMasterAPI, 'get_item'],
            'DELETE':[ItemMasterAPI, 'delete_one'],
            'POST': [ItemMasterAPI, 'edit_item'],
            'status': True,
        },
        'suppliers$': {
            'GET': [SupplierMasterAPI, 'get_all'],
            'POST': [SupplierMasterAPI, 'save_supplier'],
            'status': True,
        },
        'supplier/([\d]+)$': {
            'GET': [SupplierMasterAPI, 'get_supplier'],
            'DELETE':[SupplierMasterAPI, 'delete_one'],
            'POST': [SupplierMasterAPI, 'edit_supplier'],
            'status': True,
        },
        'countries$': {
            'GET': [CountryMasterAPI, 'get_all'],
            'status': True,
        },
        'states$': {
            'GET': [StateMasterAPI, 'get_all'],
            'status': True,
        },
        'categories$': {
            'GET': [CategoryAPI, 'get_all'],
            'POST': [CategoryAPI, 'save_category'],
            'status': True,
        },
        'category/([\d]+)$': {
            'GET': [CategoryAPI, 'get_category'],
            'DELETE':[CategoryAPI, 'delete_one'],
            'POST': [CategoryAPI, 'edit_category'],
            'status': True,
        },
        'itemrackmappings$': {
            'GET': [ItemRackMappingAPI, 'get_all'],
            'POST': [ItemRackMappingAPI, 'save_mapping'],
            'status': True,
        },
        'rfqs$': {
            'GET': [RFQMasterAPI, 'get_all'],
            'POST': [RFQMasterAPI, 'save_rfq'],
            'status': True,
        },
        'rfq/([\d]+)$': {
            'GET': [RFQMasterAPI, 'get_rfq'],
            'DELETE': [RFQMasterAPI, 'delete_one'],
            'POST': [RFQMasterAPI, 'edit_rfq'],
            'status': True
        },

    }

    @staticmethod
    def parse_and_find_key(url):
        url = url.replace('/customapi/', '')
        match = [
            key for key in APISelector.class_selectors.keys() if re.match(key, url)]
        if not match:
            return None
        return match[0]

    @staticmethod
    def get(url, method):
        matched_key = APISelector.parse_and_find_key(url)
        return APISelector.class_selectors.get(matched_key, {}).get(method, [])

    @staticmethod
    def get_configuration(url):
        matched_key = APISelector.parse_and_find_key(url)
        return APISelector.class_selectors.get(matched_key, {})
