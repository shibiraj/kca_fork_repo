from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from views import *
admin.autodiscover()


urlpatterns = patterns('',

    url(r'^([a-zA-Z]+)$', csrf_exempt(ResourcesAPIView.as_view()), name='resources_api_view'),
    url(r'^([a-zA-Z]+)/([\d]+)$', csrf_exempt(ResourceAPIView.as_view()), name='resource_api_view'),
    url(r'^([a-zA-Z]+)/([\d]+)/([a-zA-Z]+)$', csrf_exempt(AssociatedResourcesAPIView.as_view()), name='associated_resources_api_view'),
    url(r'^([a-zA-Z]+)/([\d]+)/([a-zA-Z]+)/([\d]+)$', csrf_exempt(AssociatedResourceAPIView.as_view()), name='associated_resource_api_view'),
)
