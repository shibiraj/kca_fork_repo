USER_CREATE_MSG = '''
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        Hi {{registered_user.user.username}},
        <br clear="all" />
        <br clear="all" />
        Welcome to {{company_name}}.
        <br clear="all" />
        Your {{company_name}} account has been created. Please note your username and password given below;
        <br clear="all" />
        Username : <b>{{registered_user.user.username}}</b>
        <br clear="all" />
        Password : <b>{{raw_password}}</b>
        <br clear="all" />
        <br clear="all" />

        Click {{login_link}} to Login
        <br clear="all" />
        <br clear="all" />

        Thank you,
        <br clear="all" />
        {{company_name}}
    </body>
</html>
'''