from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from administration.models import UserMaster, UserRights

class Manager(object):

    @staticmethod
    def get_logged_in_user(user):
        # cache_key = MemcacheKey.LOGGED_IN_EMPLOYEE % user.id
        # logged_in_employee = cache.get(cache_key)
        # if not logged_in_employee:
        #     try:
        #         logged_in_employee = Client.objects.get(user=user)
        #     except ObjectDoesNotExist as e:
        #         logged_in_employee = Employee.objects.get(user=user)
        #     cache.set(cache_key, logged_in_employee)
        # return logged_in_employee
        try:
            logged_in_user = User.objects.get(username=user)
            logged_in_user = UserMaster.objects.get(
                user__email=logged_in_user.email)
        except ObjectDoesNotExist as e:
            logged_in_user = User.objects.get(username=user)
            logged_in_user = UserMaster.objects.get(
                user__email=logged_in_user.email)
        # cache.set(cache_key, logged_in_employee)
        return logged_in_user

    @staticmethod
    def get_permission(logged_in_user, module_id, menu_id):
        permission = Permission()
        if menu_id and module_id and logged_in_user:
            if logged_in_user.adminuser:
                # give full permission to admin user
                permission.SAVE_PERMITTED = True
                permission.DELETE_PERMITTED = True
                permission.ADD_PERMITTED = True
                permission.VIEW_PERMITTED = True
                permission.EDIT_PERMITTED = True
            else:
                try:
                    user_right = UserRights.objects.filter(
                        user_id=logged_in_user, menu_id=menu_id).first()
                    if user_right:
                        permission.SAVE_PERMITTED = user_right.add_permitted or user_right.edit_permitted
                        permission.DELETE_PERMITTED = user_right.delete_permitted
                        permission.ADD_PERMITTED = user_right.add_permitted
                        permission.VIEW_PERMITTED = user_right.view_permitted
                        permission.EDIT_PERMITTED = user_right.edit_permitted
                except Exception, e:
                    print e
        return permission


class Permission():

    def __init__(self, add_permitted=False, edit_permitted=False, delete_permitted=False, view_permitted=False, save_permitted=False):
        self.ADD_PERMITTED = add_permitted
        self.EDIT_PERMITTED = edit_permitted
        self.DELETE_PERMITTED = delete_permitted
        self.VIEW_PERMITTED = view_permitted
        self.SAVE_PERMITTED = save_permitted
