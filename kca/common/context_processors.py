import copy
from django.contrib.auth.models import AnonymousUser
from .permission_check import Manager
from django.conf import settings
from administration.models import BranchMaster, ModuleMaster, UserRights, UserMaster
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models import Q


def logged_in_employee(request):
    if isinstance(request.user, AnonymousUser):
        return {'logged_in_employee': None}
    try:
        return {'logged_in_employee': Manager.get_logged_in_user(request.user)}
    except:
        return {'logged_in_employee': None}


def default(request):
    if request.user.id is not None:
        modules = []
        branches = []
        menu_details = []
        default_branch = None
        default_module = None
        default_branch_name = ''
        default_module_name = ''
        usermaster = Manager.get_logged_in_user(request.user)

        if usermaster.adminuser:
            # handles the admin user

            # branch details
            default_branch = usermaster.default_branch_id
            if not default_branch:
                # handles if default branch does not exists
                default_branch = usermaster.branches.order_by(
                    'branch_name').first()
            branches = BranchMaster.objects.all().values(
                'branch_id', 'branch_name')

            # modules details
            default_module = usermaster.default_module_id
            if not default_module:
                default_module = usermaster.modules.filter(active=True).order_by(
                    'module_name').first()
            modules = ModuleMaster.objects.filter(active=True).all().values(
                'module_id', 'module_name')
            if default_module:
                # menu details of default module
                parent_menus = default_module.menudetails_set.filter(
                    active=True, parent_id=None).order_by('sort_order').all()
                for parent in parent_menus:
                    menu_details.append(
                        {'menu': parent, 'childs': get_childs(parent, default_module.module_name.lower(), usermaster)})

        else:
            # handles normal users
            # details of branches under the user
            default_branch = usermaster.default_branch_id
            if not default_branch in usermaster.branches.all():
                default_branch = usermaster.branches.order_by(
                    'branch_name').first()
                if default_branch:
                    usermaster.default_branch_id = default_branch
                    usermaster.save()
            if default_branch:
                for _branch in usermaster.branches.exclude(branch_id=default_branch.branch_id).all():
                    branches.append(
                        {'branch_id': _branch.branch_id, 'branch_name': _branch.branch_name})

            # details of modules under the user
            default_module = usermaster.default_module_id
            if not default_module in usermaster.modules.all():
                # set new module
                default_module = usermaster.modules.filter(active=True).order_by(
                    'module_name').first()
                if default_module:
                    usermaster.default_module_id = default_module
                    usermaster.save()
            if default_module:
                for _module in usermaster.modules.exclude(module_id=default_module.module_id).filter(active=True).all():
                    modules.append(
                        {'module_id': _module.module_id, 'module_name': _module.module_name})

                # menu details of default module (active menus, sorted order)
                menu_list = default_module.menudetails_set.filter(
                    active=True).order_by('sort_order').all()

                parent_menus = default_module.menudetails_set.filter(
                    active=True, parent_id=None).order_by('sort_order').all()

                for parent in parent_menus:
                    if UserRights.objects.filter(user_id=usermaster.pk, menu_id=parent.menu_id).filter(Q(add_permitted=True) | Q(edit_permitted=True) | Q(delete_permitted=True) | Q(view_permitted=True)).first():
                        menu_details.append(
                            {'menu': parent, 'childs': get_childs(parent, default_module.module_name.lower(), usermaster)})
        print '##################################'
        for m in menu_details:
            print m['menu'].linkpage

        if default_branch:
            default_branch_name = default_branch.branch_name
            request.session[
                'default_branch_id'] = default_branch.branch_id

        if default_module:
            default_module_name = default_module.module_name
            request.session[
                'default_module_id'] = default_module.module_id

        context = {
            'user_name': usermaster.user_name,
            'default_branch_name': default_branch_name,
            'default_module_name': default_module_name,
            'modules': modules,
            'menu_details': menu_details,
            'branches': branches,
        }
        return context
    return {}


def get_childs(menu, module_name, user):
    menu_list = []
    for _menu in menu.menudetails_set.filter(active=True).order_by('sort_order').all():
        if user.adminuser or UserRights.objects.filter(user_id=user.pk, menu_id=_menu.menu_id).filter(Q(add_permitted=True) | Q(edit_permitted=True) | Q(delete_permitted=True) | Q(view_permitted=True)).first():
            _menu_details = {'menu': _menu}
            _menu_details['childs'] = get_childs(_menu, module_name, user)
            menu_list.append(_menu_details)
    return copy.deepcopy(menu_list)
