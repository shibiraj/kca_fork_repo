import json
from django.core import serializers
from kca.common.helpers import paginator
from kca.common.permission_check import Permission, Manager
from kca.common.constants import STARTING_PAGE, DEFAUT_PER_PAGE
from administration.models import UserRights
from django.db import IntegrityError, transaction

class JSONParser(object):

    def get(self, data):
        return json.loads(data)

    def respond(self, **data):
        return json.dumps(data)


class ModuleAPI(JSONParser):
    model = None
    relations_to_serialize = {}
    extras = []

    def _can_delete(self, id):
        return {
            'success': True,
            'errors': None
        }

    def get_all(self, page_no=STARTING_PAGE, per_page=DEFAUT_PER_PAGE, show_all=0, **search_kwargs):
        objects = self.prepare_search(
            self.model.objects.all(), **search_kwargs)
        prev_page_no = 0
        next_page_no = 0
        current_page_number = 0
        num_of_pages = objects.count()
        if not int(show_all):
            pagination_values = paginator(objects, page_no, per_page)
            objects = pagination_values['objects']
            prev_page_no = pagination_values['prev_page_no']
            next_page_no = pagination_values['next_page_no']
            current_page_number = pagination_values['current_page_number']
            num_of_pages = pagination_values['num_of_pages']
        serialized_objects = self.get(
            serializers.serialize(
                'json',
                objects,
                relations=self.relations_to_serialize,
                extras=self.extras
            )
        )
        return self.respond(
            objects=serialized_objects,
            previous_page_number=prev_page_no,
            next_page_number=next_page_no,
            current_page_number=current_page_number,
            num_of_pages=num_of_pages,
            per_page=per_page
        )

    def get_one(self, id):
        object_to_get = self.model.objects.filter(id__exact=id).first()
        serialized_object = {}
        if object_to_get:
            serialized_object = self.get(
                serializers.serialize(
                    'json',
                    [object_to_get],
                    relations=self.relations_to_serialize,
                    extras=self.extras
                )
            )
            return self.respond(success=True, object_to_get=serialized_object)
        return self.respond(success=False)

    @transaction.atomic
    def delete_one(self, id, **kwargs):
        try:
            with transaction.atomic():
                object_to_get = self.model.objects.filter(pk__exact=id).first()
                object_to_get.delete()
                self.delete_trigger(**kwargs)
                return self.respond(success=True)
        except IntegrityError:
            form_errors = {
                'integrity_error': 'Deletion is not possible due to usage in other forms',
            }
            return self.respond(success=False, form_errors=form_errors)

    def prepare_search(self, objects, **search_kwargs):
        '''child classes needs to implement this to provide search, sort and filter functionalities.
            @see ProjectAPI
        '''
        return objects

    def delete_trigger(self, **kwargs):
        return

    def update_trigger(self, old_object, new_objects, **kwargs):
        return


class PasswordAPI(ModuleAPI):

    def change_password(self, form_values):
        print 'form values', form_values
        from django.contrib.auth.forms import PasswordChangeForm
        print 'self.httpRequest.user', self.httpRequest.user
        form = PasswordChangeForm(user=self.user, data=form_values)
        if form.is_valid():
            form.save()
            return self.respond(success=True)
        print form.errors
        return self.respond(success=False, errors=form.errors)
