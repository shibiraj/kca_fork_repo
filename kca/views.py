from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from administration.models import BranchMaster, ModuleMaster
from django.contrib.auth.views import login, password_change
from django.template import RequestContext


class HomeView(View):
    template_name = 'kca/home.html'

    def get(self, request):
        return render(request, self.template_name, context_instance=RequestContext(request))


class ChangePassword(View):
    template_name = 'registration/change_password.html'

    def get(self, request):
        from django.contrib.auth.forms import PasswordChangeForm
        context = {
            'password_change_form': PasswordChangeForm(user=request.user)
        }
        return render(request, self.template_name, context)


class SetBranch(View):

    def get(self, request, id):
        branch_id = int(id)
        user = User.objects.get(pk=request.user.id)
        usermaster = user.usermaster
        def_branch = BranchMaster.objects.get(branch_id=branch_id)
        usermaster.default_branch_id = def_branch
        usermaster.save()
        request.session[
            'default_branch_id'] = branch_id
        return redirect('home')

    def post(self, request, id):
        pass


class SetModule(View):

    def get(self, request, id):
        module_id = int(id)
        user = User.objects.get(pk=request.user.id)
        usermaster = user.usermaster
        def_module = ModuleMaster.objects.get(module_id=module_id)
        usermaster.default_module_id = def_module
        usermaster.save()
        request.session[
            'default_module_id'] = module_id
        return redirect('home')

    def post(self, request, id):
        pass
