function showFormErrors(jsonObj, formId) {
	console.log(jsonObj);
	console.log(formId);
    $.each($('#' + formId + ' span.error'), function (index, element) {
        if (jsonObj[$(element).attr('name')]) {
            $(element).text(jsonObj[$(element).attr('name')][0]);
        }
    });
}

function clearFormErrors(formId) {
    $.each($('#' + formId + ' span.error'), function (index, element) {
        $(element).text('');
    });
}