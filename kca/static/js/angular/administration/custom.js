var administrationApp = angular.module('administration', []);

administrationApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});


function dateconverter(date) {
    date = date.split("/");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "-" + month + "-" + day);
}

function revertdateconverter(date) {
    date = date.split("-");
    day = date[0];
    month = date[1];
    year = date[2];
    return (year + "/" + month + "/" + day);
}

function datetimespliter(datetime) {
    var date = datetime.split("T");
    return (date[0]);
}

administrationApp.controller('companyController', function($scope, $http, $interval, $timeout) {
    
    $scope.clearForm = function() {
        $('#compname').val('');
        $('#addr1').val('');
        $('#addr2').val('');
        $('#addr3').val('');
        $('#phone').val('');
        $('#raddr1').val('');
        $('#id_company_rptaddrline2').val('');
        $('#id_company_logo').val('');
    }

});

administrationApp.controller('financialyearController', function($scope, $http, $interval, $timeout) {
    $scope.financialyears = new Array()
    $scope.search_text = '';
    $scope.count = 0;
    $scope.edit_mode = false;
    $scope.view_mode = false;
    $scope.get_financialyears = function(page_no, per_page, no_of_item) {
        $http.get(
            // '/api/administration/financialyears/',
            '/customapi/financialyears', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.financialyears = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_financialyears();
    $scope.nextPage = function() {
        $scope.get_financialyears($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_financialyears($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_financialyears($scope.cur_page, $scope.per_page);
    }
    $scope.save_financialyear = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";
        $scope.fin_year_code_error = "";
        $scope.startdate_error = "";
        $scope.enddate_error = "";
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var fin_year_code = $('#fyc').val();
        var is_valid = 1;
        if (fin_year_code == "") {
            $scope.fin_year_code_error = "* This field is required";
            is_valid = 0;
        }
        if (startdate == "") {
            $scope.startdate_error = "* This field is required";
            is_valid = 0;
        }
        if (enddate == "") {
            $scope.enddate_error = "* This field is required";
            is_valid = 0;
        } else if (startdate > enddate) {
            $scope.enddate_error = "* Startdate should be less than enddate";
            is_valid = 0;
        }
        var startdate = dateconverter($('#startdate').val());
        var enddate = dateconverter($('#enddate').val());
        if (!$scope.edit_mode) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'fin_year_code': fin_year_code,
                    'startdate': startdate,
                    'enddate': enddate,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/financialyears',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Financialyear added successfully";
                        $scope.get_financialyears();
                    } else {
                        $scope.alert_failed = "Failed to add Financialyear";
                        $scope.fin_year_code_error = data.form_errors.fin_year_code;
                        $scope.get_financialyears();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'fin_year_code': fin_year_code,
                    'startdate': startdate,
                    'enddate': enddate,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/financialyear/' + $scope.financialyear_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Financialyear Edited successfully";
                        $scope.get_financialyears();
                        $scope.fyc = "";
                        $scope.startdate = "";
                        $scope.enddate = "";
                        $scope.edit_mode = false;

                    } else {
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Financialyear";
                        $scope.fin_year_code_error = data.form_errors.fin_year_code;
                        $scope.get_financialyears();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }
    $scope.clear_form_fields = function() {
        $scope.fyc = "";
        $scope.startdate = "";
        $scope.enddate = "";
    }

    $scope.delete_financialyear = function(id) {
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this financialyear?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/financialyear/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "Financialyear Deleted successfully";
                        $.each($scope.financialyears, function(index, financialyears) {
                            if (financialyears.pk == id) {
                                $scope.financialyears.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.alert_failed = "Failed to delete Financialyear";
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.edit_financialyear = function(id) {
        $scope.financialyear_id = id;
        $http.get(
            '/customapi/financialyear/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = true;
            $scope.view_mode = false;
            console.log(data.objects);
            response = data.objects;
            $scope.fyc = response[0].fields.fin_year_code;
            $scope.startdate = revertdateconverter(datetimespliter(response[0].fields.starts_from));
            $scope.enddate = revertdateconverter(datetimespliter(response[0].fields.starts_to));
        });
    }

    $scope.view_financialyear = function(id) {
        $scope.financialyear_id = id;
        $http.get(
            '/customapi/financialyear/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit_mode = false;
            $scope.view_mode = true;
            response = data.objects;
            $scope.fyc = response[0].fields.fin_year_code;
            $scope.startdate = revertdateconverter(datetimespliter(response[0].fields.starts_from));
            $scope.enddate = revertdateconverter(datetimespliter(response[0].fields.starts_to));
        });
    }

});


administrationApp.controller('branchController', function($scope, $http, $interval, $timeout) {
    $scope.branches = new Array()
    $scope.search_text = '';
    $scope.view_mode = false;
    $scope.edit_mode = false;

    $scope.get_branches = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/branches', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            if (data.objects) {
                $scope.branches = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {})
    }
    $scope.get_branches();
    $scope.nextPage = function() {
        $scope.get_branches($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_branches($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_branches($scope.cur_page, $scope.per_page);
    }
    $scope.save_branch = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";
        $scope.branch_code_error = "";
        $scope.branch_name_error = "";
        $scope.head_office_exists_error = "";
        var branch_code = $('#branch_code').val();
        var branch_name = $('#branch_name').val();
        var branch_address1 = $('#add1').val();
        var branch_address2 = $('#add2').val();
        var branch_address3 = $('#add3').val();
        var branch_phone = $('#branch_phone').val();
        var branch_rptaddrline1 = $('#radd1').val();
        var branch_rptaddrline2 = $('#radd2').val();
        var is_head_office = $scope.is_head_office;

        var is_valid = true;
        if (branch_code == "") {
            $scope.branch_code_error = "* This field is required";
            is_valid = false;
        }
        if (branch_name == "") {
            $scope.branch_name_error = "* This field is required";
            is_valid = false;
        }

        if (!$scope.edit_mode) {
            // add operation
            if (is_valid) {
                $scope.values_to_pass = {
                    'branch_code': branch_code,
                    'branch_name': branch_name,
                    'branch_address1': branch_address1,
                    'branch_address2': branch_address2,
                    'branch_address3': branch_address3,
                    'branch_phone': branch_phone,
                    'branch_rptaddrline1': branch_rptaddrline1,
                    'branch_rptaddrline2': branch_rptaddrline2,
                    'is_head_office': is_head_office,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/branches', [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Branch added successfully";
                        $scope.get_branches();
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                            $scope.clear_form_fields();
                        }, 3000);
                    } else {
                        $scope.branch_code_error = data.form_errors.branch_code_error;
                        $scope.branch_name_error = data.form_errors.branch_name_error;
                        $scope.head_office_exists_error = data.form_errors.head_office_exists_error;
                    }
                }).error(function() {})
            } else {
                $scope.alert_failed = "Failed to add branch";
            }
        } else {
            // edit operation
            if (is_valid) {
                $scope.values_to_pass = {
                    'branch_code': branch_code,
                    'branch_name': branch_name,
                    'branch_address1': branch_address1,
                    'branch_address2': branch_address2,
                    'branch_address3': branch_address3,
                    'branch_phone': branch_phone,
                    'branch_rptaddrline1': branch_rptaddrline1,
                    'branch_rptaddrline2': branch_rptaddrline2,
                    'is_head_office': is_head_office,
                    'data_add_mac': location.host,
                }
                $http.post(
                    '/customapi/branch/' + $scope.branch_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Branch Edited successfully";
                        $scope.edit_mode = false;
                        $scope.get_branches();
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                            $scope.clear_form_fields();
                        }, 3000);
                    } else {
                        $scope.alert_failed = "Failed to Edit Branch";
                        $scope.branch_code_error = data.form_errors.branch_code_error;
                        $scope.branch_name_error = data.form_errors.branch_name_error;
                        $scope.head_office_exists_error = data.form_errors.head_office_exists_error;
                    }
                }).error(function() {})
            } else {
                $scope.alert_failed = "Failed to add Financialyear";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }
    $scope.edit_branch = function(id) {
        $scope.view_mode = false;
        $scope.branch_id = id;
        $http.get(
            '/customapi/branch/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            response = data.objects;
            console.log(response[0]);
            $scope.branch_code = response[0].fields.branch_shortname;
            $scope.branch_name = response[0].fields.branch_name;
            $scope.add1 = response[0].fields.branch_addressline1;
            $scope.add2 = response[0].fields.branch_addressline2;
            $scope.add3 = response[0].fields.branch_addressline3;
            $scope.branch_phone = response[0].fields.branch_phone;
            $scope.radd1 = response[0].fields.branch_rptaddrline1;
            $scope.radd2 = response[0].fields.branch_rptaddrline2;
            $scope.is_head_office = response[0].fields.is_corporate_office;
            $scope.edit_mode = true;
        });
    }
    $scope.view_branch = function(id) {
        $scope.edit_mode = false;
        $scope.branch_id = id;
        $http.get(
            '/customapi/branch/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            response = data.objects;
            console.log(response[0]);
            $scope.branch_code = response[0].fields.branch_shortname;
            $scope.branch_name = response[0].fields.branch_name;
            $scope.add1 = response[0].fields.branch_addressline1;
            $scope.add2 = response[0].fields.branch_addressline2;
            $scope.add3 = response[0].fields.branch_addressline3;
            $scope.branch_phone = response[0].fields.branch_phone;
            $scope.radd1 = response[0].fields.branch_rptaddrline1;
            $scope.radd2 = response[0].fields.branch_rptaddrline2;
            $scope.is_head_office = response[0].fields.is_corporate_office;
            $scope.view_mode = true;
        });
    }
    $scope.delete_branch = function(id) {
        $scope.view_mode = false;
        $scope.edit_mode = false;
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this branch?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/branch/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "Branch Deleted successfully";
                        $.each($scope.branches, function(index, branches) {
                            if (branches.pk == id) {
                                $scope.branches.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.alert_failed = "Failed to delete Branch";
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.clear_form_fields = function() {
        $scope.branch_code = "";
        $scope.branch_name = "";
        $scope.add1 = "";
        $scope.add2 = "";
        $scope.add3 = "";
        $scope.branch_phone = "";
        $scope.radd1 = "";
        $scope.radd2 = "";
        $scope.is_head_office = "";
    }
});


administrationApp.controller('userPermissionController', function($scope, $http, $interval, $timeout) {
    $scope.users = new Array();
    $scope.module_details = new Array();
    $scope.search_text = '';
    $scope.count = 0;
    $scope.edit = 0;
    $scope.last_checked = false;
    $scope.editing_user = '';

    $scope.selectall_add = 1;
    $scope.selectall_edit = 1;
    $scope.selectall_delete = 1;
    $scope.selectall_view = 1;

    $scope.selectall_add_permissions = function(module_name, permission, checkbox_state) {
        console.log('before');
        console.log($scope.module_details);
        if ($scope.module_details) {
            for (var i = 0; i < $scope.module_details.length; i++) {
                if ($scope.module_details[i].module_name == module_name) {
                    for (j = 0; j < $scope.module_details[i].menus.length; j++) {
                            if (checkbox_state == true){
                                $scope.module_details[i].menus[j][permission] = true;
                            }
                            else{
                                $scope.module_details[i].menus[j][permission] = false;   
                            }
                    }
                    break;
                }
            }
        }
        console.log('afters=');
        console.log($scope.module_details);
    };

    $scope.toggleSelection = function(module_id, menu_id, permission_type) {
        if ($scope.module_details) {
            for (var i = 0; i < $scope.module_details.length; i++) {
                if ($scope.module_details[i].module_id == module_id) {
                    for (var j = 0; j < $scope.module_details[i].menus.length; j++) {
                        if ($scope.module_details[i].menus[j].menu_id == menu_id) {
                            switch (permission_type) {
                                case "add_permitted":
                                    $scope.module_details[i].menus[j].add_permitted = !$scope.module_details[i].menus[j].add_permitted;
                                    break;
                                case "edit_permitted":
                                    $scope.module_details[i].menus[j].edit_permitted = !$scope.module_details[i].menus[j].edit_permitted;
                                    break;
                                case "delete_permitted":
                                    $scope.module_details[i].menus[j].delete_permitted = !$scope.module_details[i].menus[j].delete_permitted;
                                    break;
                                case "view_permitted":
                                    $scope.module_details[i].menus[j].view_permitted = !$scope.module_details[i].menus[j].view_permitted;
                                    break;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
    };


    $scope.get_users = function(page_no, per_page, no_of_item) {
        $http.get(
            '/customapi/activeusers', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.users = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_users();
    $scope.nextPage = function() {
        $scope.get_users($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_users($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.save_permissions = function() {
        $scope.alert_success = "";
        $scope.alert_failed = "";

        $http.post(
            '/customapi/userpermission/' + $scope.editing_user,
            $scope.module_details, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }, {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success) {
                console.log(data);
                $scope.alert_success = "Permissions successfully updated";
                $scope.editing_user = '';
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                    $scope.module_details = [];
                }, 3000);
            } else {

            }
        }).error(function() {

        })
    }

    $scope.edit_permission = function(id) {
        $http.get(
            '/customapi/userpermission/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            $scope.editing_user = id;
            response = JSON.parse(data.objects);
            $scope.module_details = response;
            console.log('$scope.module_details');
            console.log($scope.module_details);
        });
    }

});


administrationApp.controller('passwordController', function($scope, $http, $interval, $timeout) {
    $scope.alert_success = "";
    $scope.alert_failed = "";

    $scope.change_password = function() {
        $scope.values_to_pass = {
            old_password: $('#id_old_password').val(),
            new_password1: $('#id_new_password1').val(),
            new_password2: $('#id_new_password2').val()
        }
        $http.post(
            '/customapi/changepassword', [$scope.values_to_pass], {
                'responseType': 'json'
            }
        ).success(function(data) {
            if (data.success == true) {
                console.log('data');
                console.log(data);
                $scope.alert_success = "Password successfully changed, please re-login.";
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
                clearFormErrors('change_password_form');
                $('#id_old_password').val('');
                $('#id_new_password1').val('');
                $('#id_new_password2').val('');

            } else {
                showFormErrors(data.errors, 'change_password_form');
                $scope.alert_failed = "Password change failed.";
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        }).error(function(data) {});
    };
    $scope.clearForm = function() {
        $('#id_old_password').val('');
        $('#id_new_password1').val('');
        $('#id_new_password2').val('');
        clearFormErrors('change_password_form');
    }

});



administrationApp.controller('Administartion_UserController', function($scope, $http, $interval, $timeout) {
    $scope.branches = new Array();
    $scope.modules = new Array();
    $scope.users = new Array();
    $scope.alert_success = "";
    $scope.alert_failed = "";
    $scope.username = "";
    $scope.address1 = "";
    $scope.address2 = "";
    $scope.phone = "";
    $scope.email = "";
    $scope.is_active = " ";
    $scope.email_error = " ";
    $scope.username_error = " ";

    $scope.edit_mode = false;
    $scope.view_mode = false;

    $scope.search_text = '';
    $scope.count = 0;

    $scope.selectall_branch = 1;
    $scope.selectall_module = 1;

    $scope.get_branches = function() {
        $http.get(
            // '/api/administration/financialyears/',
            '/customapi/branches', {
                'responseType': 'json',

            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.branches = data.objects;
                console.log
            }

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_branches();
    $scope.get_modules = function() {
        $http.get(
            // '/api/administration/financialyears/',
            '/customapi/modules', {
                'responseType': 'json',

            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.modules = data.objects;
                console.log
            }

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_modules();
    $scope.save_user = function() {
            $scope.email_error = " ";
            $scope.username_error = " ";
            var is_valid = 1;
            if ($scope.username == "") {
                $scope.username_error = "* This field is required";
                is_valid = 0;
            }
            var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;

            if (!emailFilter.test($scope.email)) {
                $scope.email_error = 'Please enter a valid e-mail address.';
                is_valid = 0;
            }
            if ($scope.email == "") {
                $scope.email_error = "* This field is required";
                is_valid = 0;
            }
            if (!$scope.edit_mode) {
                if (is_valid) {
                    $scope.values_to_pass = {
                        'username': $scope.username,
                        'address1': $scope.address1,
                        'address2': $scope.address2,
                        'phone': $scope.phone,
                        'email': $scope.email,
                        'data_add_mac': "",
                    }
                    $http.post(
                        // '/api/administration/financialyears',
                        '/customapi/users',

                        [$scope.values_to_pass, $scope.branch_selection, $scope.module_selection], {
                            'responseType': 'json'
                        }
                    ).success(function(data) {
                        if (data.success) {
                            $scope.alert_success = "User added successfully";
                            $scope.get_users();
                        } else {
                            $scope.alert_failed = "Failed to add user";
                            $scope.email_error = data.form_errors.email_error;
                        }
                        $timeout(function() {
                            $scope.alert_failed = "";
                            $scope.alert_success = "";
                        }, 3000);
                    }).error(function() {
                        // alert("failed");
                    })
                } else {
                    $scope.alert_failed = "Failed to add user";
                }
            } else {
                if (is_valid) {
                    $scope.values_to_pass = {
                        'username': $scope.username,
                        'address1': $scope.address1,
                        'address2': $scope.address2,
                        'phone': $scope.phone,
                        'email': $scope.email,
                        'data_add_mac': "",
                        'is_active': $scope.is_active,
                    }
                    $http.post(
                        // '/api/administration/financialyears',
                        '/customapi/user/' + $scope.user_id,

                        [$scope.values_to_pass, $scope.branch_selection, $scope.module_selection], {
                            'responseType': 'json'
                        }
                    ).success(function(data) {
                        console.log(data);
                        if (data.success) {
                            $scope.alert_success = "User Edited successfully";
                            $scope.clear_form_fields();
                            $scope.get_users();
                        } else {
                            $scope.alert_failed = "Failed to Edit User";
                            $scope.email_error = data.form_errors.email_error;
                        }

                    }).error(function() {
                        // alert("failed");
                    })
                } else {
                    $scope.alert_failed = "Failed to Edit User";
                }

            }
            $timeout(function() {
                $scope.alert_failed = "";
                $scope.alert_success = "";
            }, 3000);

        }
        /* Creating Selected Modules List*/
    $scope.module_selection = [];
    $scope.mod_toggleSelection = function toggleSelection(module_id) {
        var idx = $scope.module_selection.indexOf(module_id);
        // is currently selected
        if (idx > -1) {
            $scope.module_selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.module_selection.push(module_id);
        }
        console.log($scope.module_selection);
    };
    /* Creating Selected Branches List*/
    $scope.branch_selection = [];
    $scope.br_toggleSelection = function toggleSelection(module_id) {
        var idx = $scope.branch_selection.indexOf(module_id);
        // is currently selected
        if (idx > -1) {
            $scope.branch_selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.branch_selection.push(module_id);
        }
        console.log($scope.branch_selection);
    };

    $scope.selectall_branches = function() {
        $scope.branch_selection = [];
        if ($scope.selectall_branch) {
            angular.forEach($scope.branches, function(item) {
                $scope.branch_selection.push(item.pk)
            });
            $scope.selectall_branch = "";
            console.log($scope.branch_selection);
        } else {
            $scope.branch_selection = [];
            $scope.selectall_branch = 1;
            console.log($scope.branch_selection);
        }

    }
    $scope.selectall_modules = function() {
        $scope.module_selection = [];
        if ($scope.selectall_module) {
            angular.forEach($scope.modules, function(item) {
                $scope.module_selection.push(item.pk)
            });
            $scope.selectall_module = "";
            console.log($scope.module_selection);
        } else {
            $scope.module_selection = [];
            $scope.selectall_module = 1;
            console.log($scope.module_selection);
        }

    }

    $scope.get_users = function(page_no, per_page, no_of_item) {
        $http.get(
            // '/api/administration/financialyears/',
            '/customapi/users', {
                'responseType': 'json',
                'params': {
                    'page_no': page_no,
                    'per_page': per_page,
                    'search_text': $scope.search_text,
                    'show_all': no_of_item
                }
            }
        ).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            if (data.objects) {
                $scope.users = data.objects;
            }
            $scope.cur_page = data.current_page_number;
            $scope.next_page = data.next_page_number;
            $scope.prev_page = data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;

        }).error(function() {
            // alert("failed");
        })
    }
    $scope.get_users();
    $scope.nextPage = function() {
        $scope.get_users($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_users($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_users($scope.cur_page, $scope.per_page);
    }

    $scope.delete_user = function(id) {
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this user?", function(result) {
            if (result == true) {
                $http.post(
                    '/customapi/user/' + id, [], {
                        'responseType': 'json',
                        'headers': {
                            'X-HTTP-Method-Override': 'DELETE'
                        }
                    }).success(function(response) {
                    if (response.success) {
                        $scope.alert_success = "User Deleted successfully";
                        console.log($scope.users);
                        $.each($scope.users, function(index, users) {
                            if (users.pk == id) {
                                $scope.users.splice(index, 1);
                            }
                        });
                    } else {
                        // $scope.alert_failed = "Failed to delete User";
                        // alert("false");
                        $scope.alert_failed = response.form_errors.user_delete_error;
                        $scope.get_financialyears();
                    }
                });
                $timeout(function() {
                    $scope.alert_failed = "";
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.edit_user = function(id) {
        $scope.user_id = id;
        $http.get(
            '/customapi/user/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            if (data.success) {
                $scope.edit_mode = true;
                $scope.view_mode = false;
                // console.log(data.objects);
                response = data.objects;
                $scope.username = response[0].fields.user.fields.first_name;
                $scope.address1 = response[0].fields.address1;
                $scope.address2 = response[0].fields.address2;
                $scope.phone = response[0].fields.phone;
                $scope.email = response[0].fields.user.fields.email;
                // $scope.is_active = 1;
                $scope.is_active = response[0].fields.user.fields.is_active;
                $scope.module_list = response[0].fields.modules;
                $scope.branch_list = response[0].fields.branches;
                $scope.branch_selection = $scope.branch_list;
                $scope.module_selection = $scope.module_list;
                // console.log($scope.module_list);
            }

        });
    }

    $scope.view_user = function(id) {
        $scope.user_id = id;
        $http.get(
            '/customapi/user/' + id, {
                'responseType': 'json'
            }
        ).success(function(data, status, headers, config) {
            if (data.success) {
                $scope.edit_mode = false;
                $scope.view_mode = true;
                // console.log(data.objects);
                response = data.objects;
                $scope.username = response[0].fields.user.fields.first_name;
                $scope.address1 = response[0].fields.address1;
                $scope.address2 = response[0].fields.address2;
                $scope.phone = response[0].fields.phone;
                $scope.email = response[0].fields.user.fields.email;
                // $scope.is_active = 1;
                $scope.is_active = response[0].fields.user.fields.is_active;
                $scope.module_list = response[0].fields.modules;
                $scope.branch_list = response[0].fields.branches;
                $scope.branch_selection = $scope.branch_list;
                $scope.module_selection = $scope.module_list;
                // console.log($scope.module_list);
            }
        });
    }

    $scope.check_module = function(module_id) {
        return $scope.module_list.indexOf(module_id);
    }
    $scope.user_activate = function() {
        $scope.is_active = !$scope.is_active;
    }
    $scope.clear_form_fields = function() {
            $scope.username = "";
            $scope.address1 = "";
            $scope.address2 = "";
            $scope.phone = "";
            $scope.email = "";
            $scope.is_active = "";
            $scope.module_selection = [];
            $scope.branch_selection = [];
            $scope.email_error = " ";
            $scope.username_error = " ";
            $scope.alert_failed = "";
            $scope.alert_success = "";

        }
        // 	$scope.customArrayFilter = function (item) {
        // 		// alert("1");
        // 		console.log(item);
        // 		console.log($scope.modules);
        // 		for (var i=0; i<$scope.modules.length; i++) {
        // 			// console.log($scope.modules[i]);
        // 			if($scope.modules[i].pk==item.pk){
        // 				return true;
        // 			}
        //     // Iterates over numeric indexes from 0 to 5, as everyone expects.
        // }
        //   };

});




// Store Master
administrationApp.controller('storeMasterListController', function ($scope, $http, $interval,$timeout) {
	$scope.stores = new Array()
	$scope.search_text = '';
  	$scope.count = 0;
  	$scope.edit = 0;
  	$scope.store_code = "";
	$scope.store_name = "";
	$scope.remarks = "";
	$scope.alert_success = "";
	$scope.alert_failed = "";
	$scope.store_code_error = "";
	$scope.store_name_error = "";
	$scope.get_stores = function(page_no, per_page,no_of_item){
		$http.get(
		// '/api/administration/financialyears/',
		'/customapi/stores',
		{
			'responseType' : 'json',
			'params' : {
				'page_no':page_no,	
				'per_page' : per_page,
				'search_text':$scope.search_text,
      			'show_all': no_of_item
			}
		}
		).success(function(data){
			// this callback will be called asynchronously
       		// when the response is available
       		if(data.objects){
       			$scope.stores = data.objects;
       		}
       		$scope.cur_page=data.current_page_number;
		    $scope.next_page=data.next_page_number;
		    $scope.prev_page=data.previous_page_number;
		    $scope.last_page = data.last_page;
		    $scope.total = data.num_of_pages;
		    $scope.per_page = data.per_page;

		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_stores();
	$scope.nextPage = function () {
        $scope.get_stores($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function() {
        $scope.get_stores($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }
    $scope.search = function() {
        $scope.get_stores($scope.cur_page, $scope.per_page);
    }


    $scope.save_store = function() {

        var is_valid = 1;
        if ($scope.store_code == "") {
            $scope.store_code_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.store_name == "") {
            $scope.store_name_error = "* This field is required";
            is_valid = 0;
        }
        if ($scope.edit != 1) {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/stores',

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store added successfully";
                        $scope.clear_form_fields();
                        $scope.get_stores();
                    } else {
                        $scope.alert_failed = "Failed to add Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                        $scope.get_stores();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        } else {
            if (is_valid == 1) {
                $scope.values_to_pass = {
                    'store_code': $scope.store_code,
                    'store_name': $scope.store_name,
                    'remarks': $scope.remarks,
                    'data_add_mac': location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/store/' + $scope.store_id,

                    [$scope.values_to_pass], {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if (data.success) {
                        $scope.alert_success = "Store Edited successfully";
                        $scope.get_stores();
                        $scope.store_code = "";
                        $scope.store_name = "";
                        $scope.remarks = "";
                        $scope.edit = 0;
                        $scope.get_stores();

                    } else {
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Store";
                        $scope.store_code_error = data.form_errors.store_code;
                        $scope.store_name_error = data.form_errors.store_name;
                        // $scope.get_financialyears();
                    }
                }).error(function() {
                    // alert("failed");
                })
            } else {
                $scope.alert_failed = "Failed to add Store";
            }
        }
        $timeout(function() {
            $scope.alert_failed = "";
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_store = function(id){
    	$scope.clear_form_fields();
		$scope.store_id = id;
  		$http.get(
		'/customapi/store/'+id,
			{
				'responseType' : 'json'
		    }
		).success(function(data, status, headers, config) {
			$scope.edit = 1;
			console.log(data.objects);
			response = data.objects;
			$scope.store_code = response[0].fields.store_code;
			$scope.store_name = response[0].fields.store_name;
			$scope.remarks = response[0].fields.remarks;
			});		
  	}

  	$scope.delete_store = function(id){
	    $scope.loading = true;
	    bootbox.confirm("Are sure want to delete this store?", function(result) {
	        if(result==true){
			    $http.post(
			    '/customapi/store/'+id,
			    [],
			    {
			      'responseType' : 'json',
			      'headers': {'X-HTTP-Method-Override': 'DELETE'}
			    }).success(function(response){
			    	if (response.success) {
			        	$scope.alert_success = "Store Deleted successfully";
			            $.each($scope.stores, function (index, store) {
			            	if (store.pk == id) {
			                  $scope.stores.splice(index,1);
			            	}
			        	});
			    	}	
			    	else{
			    		$scope.alert_failed = "Failed to delete Store";
			    	}
		     	});
		     	$timeout( function(){ 
			    	$scope.alert_failed = ""; 
			    	$scope.alert_success = "";
			    }, 3000);
	        }
        });
  	}

    $scope.clear_form_fields = function(){
    	$scope.store_code = "";
		$scope.store_name = "";
		$scope.remarks = "";
		$scope.store_code_error = "";
		$scope.store_name_error = "";
    }
});


// RackMaster
administrationApp.controller('rackMasterListController', function ($scope, $http, $interval,$timeout) {
	$scope.store_id = "";
	$scope.search_text = "";
	$scope.rack_location = "";
	$scope.remarks = "";
    $scope.edit = 0;
	$scope.get_currentbranch_store = function(page_no, per_page,no_of_item){
		$http.get(
		// '/api/administration/financialyears/',
		'/customapi/stores',
		{
			'responseType' : 'json',
			'params' : {
				'page_no':page_no,	
				'per_page' : per_page,
				'search_text':$scope.search_text,
      			'show_all': no_of_item,
      			'currentbranch_store' : 1,
			}
		}
		).success(function(data){
			// this callback will be called asynchronously
       		// when the response is available
       		if(data.objects){
       			$scope.stores = data.objects;
       		}
       		$scope.cur_page=data.current_page_number;
		    $scope.next_page=data.next_page_number;
		    $scope.prev_page=data.previous_page_number;
		    $scope.last_page = data.last_page;
		    $scope.total = data.num_of_pages;
		    $scope.per_page = data.per_page;
            $scope.store_id = $scope.stores[0].pk;

		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_currentbranch_store();
	
	$scope.get_racks = function(page_no, per_page,no_of_item){
		$http.get(
		// '/api/administration/financialyears/',
		'/customapi/racks',
		{
			'responseType' : 'json',
			'params' : {
				'page_no':page_no,	
				'per_page' : per_page,
				'search_text':$scope.search_text,
      			'show_all': no_of_item
			}
		}
		).success(function(data){
			// this callback will be called asynchronously
       		// when the response is available
       		if(data.objects){
       			$scope.racks = data.objects;
       		}
       		$scope.cur_page=data.current_page_number;
		    $scope.next_page=data.next_page_number;
		    $scope.prev_page=data.previous_page_number;
		    $scope.last_page = data.last_page;
		    $scope.total = data.num_of_pages;
		    $scope.per_page = data.per_page;


		}).error(function(){
			// alert("failed");
		})
	}
	$scope.get_racks();
	$scope.nextPage = function () {
        $scope.get_racks($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
	        $scope.get_racks($scope.prev_page, $scope.per_page);
	};
	$scope.genPage = function () {
	    $scope.get_racks($scope.cur_page, $scope.per_page);
	};
	$scope.genRecords = function(){
	      $scope.get_racks($scope.cur_page, $scope.per_page);
	}
	$scope.search = function () {
	    $scope.get_racks($scope.cur_page, $scope.per_page);
	}

	$scope.save_rack = function(){
		$scope.rack_location_error = " ";
		$scope.store_id_error = " ";
		var is_valid = 1;
		if($scope.store_id==""){
			$scope.store_id_error = "* This field is required";
			is_valid = 0;
		}
		if($scope.rack_location == ""){
			$scope.rack_location_error = "* This field is required";
			is_valid = 0;
		}
		if($scope.edit!=1){
			if(is_valid==1){
				$scope.values_to_pass = {
					'store_id' : $scope.store_id,
					'rack_location' : $scope.rack_location,
					'remarks' : $scope.remarks,
					'data_add_mac' : location.host,
				}
				$http.post(
		            // '/api/administration/financialyears',
		            '/customapi/racks',

		            [$scope.values_to_pass],
		            {
		                'responseType': 'json'
		            }
		      	).success(function(data) {
		       		if(data.success){
		       			$scope.alert_success = "Rack added successfully";
		       			$scope.clear_form_fields();
		       			$scope.get_racks();
		       		}
		       		else{
		       			$scope.alert_failed = "Failed to add Rack";
		       			$scope.rack_location_error = data.form_errors.rack_location;
		       			$scope.get_racks();
		       		}
		       	}).error(function(){
		       		// alert("failed");
		       	})
		    }
		    else{
		    	$scope.alert_failed = "Failed to add Rack";
		    }
		}
		else{
			if(is_valid==1){
				$scope.values_to_pass = {
					'store_id' : $scope.store_id,
                    'rack_location' : $scope.rack_location,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
				}
				$http.post(
		            // '/api/administration/financialyears',
		            '/customapi/rack/'+$scope.rack_id,

		            [$scope.values_to_pass],
		            {
		                'responseType': 'json'
		            }
		      	).success(function(data) {
		       		if(data.success){
		       			$scope.alert_success = "Rack Edited successfully";
		       			$scope.store_code = "";
						$scope.store_name = "";
						$scope.remarks = "";
		       			$scope.edit = 0;
                        $scope.get_racks();
                        $scope.clear_form_fields();

		       		}
		       		else{
		       			// alert("false");
		       			$scope.alert_failed = "Failed to Edit Rack";
		       			$scope.rack_location_error = data.form_errors.rack_location;
		       			// $scope.get_financialyears();
		       		}
		       	}).error(function(){
		       		// alert("failed");
		       	})
		    }
		    else{
		    	$scope.alert_failed = "Failed to Edit Rack";
		    }
		}
	    $timeout( function(){ 
	    	$scope.alert_failed = ""; 
	    	$scope.alert_success = "";
	    }, 3000);
    }

    $scope.edit_rack = function(id){
    	$scope.clear_form_fields();
		$scope.rack_id = id;
  		$http.get(
		'/customapi/rack/'+id,
			{
				'responseType' : 'json'
		    }
		).success(function(data, status, headers, config) {
			$scope.edit = 1;
			console.log(data.objects);
			response = data.objects;
			$scope.store_id = response[0].fields.store_id.pk;
			$scope.rack_location = response[0].fields.rack_location;
			$scope.remarks = response[0].fields.remarks;
			console.log($scope.storemodel);
			});		
  	}

  	$scope.delete_rack = function(id){
	    $scope.loading = true;
	    bootbox.confirm("Are sure want to delete this rack?", function(result) {
	        if(result==true){
			    $http.post(
			    '/customapi/rack/'+id,
			    [],
			    {
			      'responseType' : 'json',
			      'headers': {'X-HTTP-Method-Override': 'DELETE'}
			    }).success(function(response){
			    	if (response.success) {
			        	$scope.alert_success = "Rack Deleted successfully";
			            $.each($scope.racks, function (index, rack) {
			            	if (rack.pk == id) {
			                  $scope.racks.splice(index,1);
			            	}
			        	});
			    	}	
			    	else{
			    		$scope.alert_failed = "Failed to delete Rack";
			    	}
		     	});
		     	$timeout( function(){ 
			    	$scope.alert_failed = ""; 
			    	$scope.alert_success = "";
			    }, 3000);
	        }
        });
  	}

    $scope.clear_form_fields = function(){
    	$scope.store_id = "";
    	$scope.rack_location = "";
    	$scope.remarks = "";
    	$scope.rack_location_error = " ";
		$scope.store_id_error = " ";
		// $scope.alert_failed = "";
		// $scope.alert_success = "";

    }
});

// UnitMaster

administrationApp.controller('unitMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit = 0;
    $scope.unit_name = "";
    $scope.get_units = function(page_no, per_page,no_of_item){
        $http.get(
        // '/api/administration/financialyears/',
        '/customapi/units',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.units = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_units();
    $scope.nextPage = function () {
        $scope.get_units($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_units($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_units($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_units($scope.cur_page, $scope.per_page);
    }

    $scope.save_unit = function(){
        $scope.unit_name_error = " ";
        var is_valid = 1;
        if($scope.unit_name==""){
            $scope.unit_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.edit!=1){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/units',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit added successfully";
                        $scope.clear_form_fields();
                        $scope.get_units();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                        // $scope.get_racks();
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Unit";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'unit_name' : $scope.unit_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    // '/api/administration/financialyears',
                    '/customapi/unit/'+$scope.unit_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Unit Edited successfully";
                        $scope.edit = 0;
                        $scope.get_units();
                        $scope.clear_form_fields();

                    }
                    else{
                        // alert("false");
                        $scope.alert_failed = "Failed to Edit Unit";
                        $scope.unit_name_error = data.form_errors.unit_name;
                        // $scope.get_financialyears();
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Unit";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_unit = function(id){
        $scope.clear_form_fields();
        $scope.unit_id = id;
        $http.get(
        '/customapi/unit/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            console.log(data.objects);
            response = data.objects;
            $scope.unit_name = response[0].fields.unit_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_unit = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this unit?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/unit/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Unit Deleted successfully";
                        $.each($scope.units, function (index, unit) {
                            if (unit.pk == id) {
                              $scope.units.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = "Failed to delete Unit";
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }
    $scope.clear_form_fields = function(){
        $scope.unit_name = "";
        $scope.remarks = "";
        $scope.unit_name_error = " ";
    }
});

// BrandMaster
administrationApp.controller('brandMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.edit = 0;
    $scope.brand_name = "";
    $scope.brand_code = "";
    $scope.remarks = "";
    $scope.get_brands = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/brands',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.brands = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_brands();
    $scope.nextPage = function () {
        $scope.get_brands($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_brands($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_brands($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_brands($scope.cur_page, $scope.per_page);
    }

    $scope.save_brand = function(){
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
        var is_valid = 1;
        if($scope.brand_code==""){
            $scope.brand_code_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.brand_name==""){
            $scope.brand_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.edit!=1){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brands',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand added successfully";
                        $scope.clear_form_fields();
                        $scope.get_brands();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'brand_code' : $scope.brand_code,
                    'brand_name' : $scope.brand_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/brand/'+$scope.brand_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Brand Edited successfully";
                        $scope.edit = 0;
                        $scope.get_brands();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Brand";
                        $scope.brand_code_error = data.form_errors.brand_code;
                        $scope.brand_name_error = data.form_errors.brand_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Brand";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_brand = function(id){
        $scope.clear_form_fields();
        $scope.brand_id = id;
        $http.get(
        '/customapi/brand/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            console.log(data.objects);
            response = data.objects;
            $scope.brand_code = response[0].fields.brand_code;
            $scope.brand_name = response[0].fields.brand_name;
            $scope.remarks = response[0].fields.remarks;
            });     
    }

    $scope.delete_brand = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this brand?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/brand/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Brand Deleted successfully";
                        $.each($scope.brands, function (index, brand) {
                            if (brand.pk == id) {
                              $scope.brands.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = "Failed to delete Brand";
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.brand_code = "";
        $scope.brand_name = "";
        $scope.remarks = "";
        $scope.brand_code_error = " ";
        $scope.brand_name_error = " ";
    }
});

// SizeMaster
administrationApp.controller('sizeMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.size_name = "";
    $scope.remarks = "";
    $scope.edit = 0;
    $scope.get_sizes = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/sizes',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.sizes = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_sizes();
    $scope.nextPage = function () {
        $scope.get_sizes($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_sizes($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_sizes($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_sizes($scope.cur_page, $scope.per_page);
    }

    $scope.save_size = function(){
        $scope.size_name_error = " ";
        var is_valid = 1;
        if($scope.size_name==""){
            $scope.size_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.edit!=1){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/sizes',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size added successfully";
                        $scope.clear_form_fields();
                        $scope.get_sizes();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Brand";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'size_name' : $scope.size_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/size/'+$scope.size_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Size Edited successfully";
                        $scope.edit = 0;
                        $scope.get_sizes();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Size";
                        $scope.size_name_error = data.form_errors.size_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Size";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_size = function(id){
        $scope.clear_form_fields();
        $scope.size_id = id;
        $http.get(
        '/customapi/size/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            console.log(data.objects);
            response = data.objects;
            $scope.size_name = response[0].fields.size_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_size = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this size?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/size/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Size Deleted successfully";
                        $.each($scope.sizes, function (index, size) {
                            if (size.pk == id) {
                              $scope.sizes.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = "Failed to delete Size";
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.size_name = "";
        $scope.remarks = "";
        $scope.size_name_error = " ";
    }
});

// PatternMaster
administrationApp.controller('patternMasterListController', function ($scope, $http, $interval,$timeout) {
    $scope.pattern_name = "";
    $scope.get_patterns = function(page_no, per_page,no_of_item){
        $http.get(
        '/customapi/patterns',
        {
            'responseType' : 'json',
            'params' : {
                'page_no':page_no,  
                'per_page' : per_page,
                'search_text':$scope.search_text,
                'show_all': no_of_item
            }
        }
        ).success(function(data){
            // this callback will be called asynchronously
            // when the response is available
            if(data.objects){
                $scope.patterns = data.objects;
            }
            $scope.cur_page=data.current_page_number;
            $scope.next_page=data.next_page_number;
            $scope.prev_page=data.previous_page_number;
            $scope.last_page = data.last_page;
            $scope.total = data.num_of_pages;
            $scope.per_page = data.per_page;


        }).error(function(){
            // alert("failed");
        })
    }
    $scope.get_patterns();
    $scope.nextPage = function () {
        $scope.get_patterns($scope.next_page, $scope.per_page);
    };
    $scope.prePage = function () {
            $scope.get_patterns($scope.prev_page, $scope.per_page);
    };
    $scope.genPage = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    };
    $scope.genRecords = function(){
          $scope.get_patterns($scope.cur_page, $scope.per_page);
    }
    $scope.search = function () {
        $scope.get_patterns($scope.cur_page, $scope.per_page);
    }

    $scope.save_pattern = function(){
        $scope.pattern_name_error = " ";
        var is_valid = 1;
        if($scope.pattern_name==""){
            $scope.pattern_name_error = "* This field is required";
            is_valid = 0;
        }
        if($scope.edit!=1){
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/patterns',

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern added successfully";
                        $scope.clear_form_fields();
                        $scope.get_patterns();
                    }
                    else{
                        $scope.alert_failed = "Failed to add Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to add Pattern";
            }
        }
        else{
            if(is_valid==1){
                $scope.values_to_pass = {
                    'pattern_name' : $scope.pattern_name,
                    'remarks' : $scope.remarks,
                    'data_add_mac' : location.host,
                }
                $http.post(
                    '/customapi/pattern/'+$scope.pattern_id,

                    [$scope.values_to_pass],
                    {
                        'responseType': 'json'
                    }
                ).success(function(data) {
                    if(data.success){
                        $scope.alert_success = "Pattern Edited successfully";
                        $scope.edit = 0;
                        $scope.get_patterns();
                        $scope.clear_form_fields();

                    }
                    else{
                        $scope.alert_failed = "Failed to Edit Pattern";
                        $scope.pattern_name_error = data.form_errors.pattern_name;
                    }
                }).error(function(){
                    // alert("failed");
                })
            }
            else{
                $scope.alert_failed = "Failed to Edit Pattern";
            }
        }
        $timeout( function(){ 
            $scope.alert_failed = ""; 
            $scope.alert_success = "";
        }, 3000);
    }

    $scope.edit_pattern = function(id){
        $scope.clear_form_fields();
        $scope.pattern_id = id;
        $http.get(
        '/customapi/pattern/'+id,
            {
                'responseType' : 'json'
            }
        ).success(function(data, status, headers, config) {
            $scope.edit = 1;
            console.log(data.objects);
            response = data.objects;
            $scope.pattern_name = response[0].fields.pattern_name;
            $scope.remarks = response[0].fields.remarks;
        });     
    }

    $scope.delete_pattern = function(id){
        $scope.loading = true;
        bootbox.confirm("Are sure want to delete this pattern?", function(result) {
            if(result==true){
                $http.post(
                '/customapi/pattern/'+id,
                [],
                {
                  'responseType' : 'json',
                  'headers': {'X-HTTP-Method-Override': 'DELETE'}
                }).success(function(response){
                    if (response.success) {
                        $scope.alert_success = "Pattern Deleted successfully";
                        $.each($scope.patterns, function (index, pattern) {
                            if (pattern.pk == id) {
                              $scope.patterns.splice(index,1);
                            }
                        });
                    }   
                    else{
                        $scope.alert_failed = "Failed to delete Pattern";
                    }
                });
                $timeout( function(){ 
                    $scope.alert_failed = ""; 
                    $scope.alert_success = "";
                }, 3000);
            }
        });
    }

    $scope.clear_form_fields = function(){
        $scope.pattern_name = "";
        $scope.remarks = "";
        $scope.pattern_name_error = " ";
    }

});
