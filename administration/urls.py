from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
                       url(r'^company$',
                           CompanyAddView.as_view(), name='administration_company'),
                       url(r'^branch$',
                           BranchView.as_view(), name='administration_branch'),
                       url(r'^financial_year$',
                           FinancialyearView.as_view(), name='administration_financial_year'),
                       url(r'^userpermission$',
                           UserPermissionView.as_view(), name='administration_user_permission'),
                       url(r'^user$',
                           UserView.as_view(), name='administration_user'),
                       )
