# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion
import administration.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BranchMaster',
            fields=[
                ('branch_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nBranchId')),
                ('branch_shortname', models.CharField(max_length=3, db_column=b'nBranchShortName')),
                ('branch_name', models.CharField(max_length=100, db_column=b'cBranchName')),
                ('branch_addressline1', models.CharField(max_length=50, null=True, db_column=b'cBranchAddressLine1', blank=True)),
                ('branch_addressline2', models.CharField(max_length=50, null=True, db_column=b'cBranchAddressLine2', blank=True)),
                ('branch_addressline3', models.CharField(max_length=50, null=True, db_column=b'cBranchAddressLine3', blank=True)),
                ('branch_phone', models.CharField(max_length=50, null=True, db_column=b'cBranchPhone', blank=True)),
                ('branch_rptaddrline1', models.CharField(max_length=50, null=True, db_column=b'cBranchRptAddrLine1', blank=True)),
                ('branch_rptaddrline2', models.CharField(max_length=50, null=True, db_column=b'cBranchRptAddrLine2', blank=True)),
                ('is_corporate_office', models.BooleanField(default=None, db_column=b'cCorporateOffice')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
            ],
            options={
                'db_table': 'tsmbranchmaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CompanyMaster',
            fields=[
                ('company_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nCompanyId')),
                ('company_name', models.CharField(max_length=100, db_column=b'cCompanyName')),
                ('company_addressline1', models.CharField(max_length=50, null=True, db_column=b'cCompanyAddressLine1', blank=True)),
                ('company_addressline2', models.CharField(max_length=50, null=True, db_column=b'cCompanyAddressLine2', blank=True)),
                ('company_addressline3', models.CharField(max_length=50, null=True, db_column=b'cCompanyAddressLine3', blank=True)),
                ('company_phone', models.CharField(max_length=50, null=True, db_column=b'cCompanyPhone', blank=True)),
                ('company_rptaddrline1', models.CharField(max_length=50, null=True, db_column=b'cCompanyRptAddrLine1', blank=True)),
                ('company_rptaddrline2', models.CharField(max_length=50, null=True, db_column=b'cCompanyRptAddrLine2', blank=True)),
                ('company_logo', models.ImageField(null=True, upload_to=administration.models.get_file_path, db_column=b'iCompanyLogo')),
                ('image_type', models.CharField(max_length=50, null=True, db_column=b'cImageType', blank=True)),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
            ],
            options={
                'db_table': 'tsmcompanymaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FinancialYear',
            fields=[
                ('fin_year_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nFinYearID')),
                ('fin_year_code', models.CharField(max_length=10, db_column=b'cFinYearCode')),
                ('starts_from', models.DateTimeField(db_column=b'dStartsFrom')),
                ('starts_to', models.DateTimeField(db_column=b'dStartsTo')),
                ('data_add_on', models.DateTimeField(auto_now=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now_add=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('company_id', models.ForeignKey(related_name='fin_year_company_relation', db_column=b'nCompanyId', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
            ],
            options={
                'db_table': 'tsmfinancialyear',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MenuDetails',
            fields=[
                ('menu_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nMenuId')),
                ('menu_name', models.CharField(max_length=100, db_column=b'cMenuName')),
                ('parent_id', models.IntegerField(null=True, verbose_name=b'self', db_column=b'nParentId', blank=True)),
                ('active', models.BooleanField(default=True, db_column=b'cActive')),
                ('linkpage', models.CharField(max_length=150, db_column=b'cLinkPage')),
                ('sort_order', models.IntegerField(db_column=b'nSortOrder')),
                ('icon_class', models.CharField(max_length=50, db_column=b'cIconClass')),
            ],
            options={
                'db_table': 'tsmmenudetails',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ModuleMaster',
            fields=[
                ('module_id', models.AutoField(serialize=False, primary_key=True, db_column=b'nModuleId')),
                ('module_name', models.CharField(max_length=50, db_column=b'cModuleName')),
                ('active', models.BooleanField(default=True, db_column=b'cActive')),
            ],
            options={
                'db_table': 'tsmmodulemaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserMaster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_name', models.CharField(max_length=150, db_column=b'cUserName')),
                ('address1', models.CharField(max_length=50, db_column=b'cAddress1', blank=True)),
                ('address2', models.CharField(max_length=50, db_column=b'cAddress2', blank=True)),
                ('phone', models.CharField(max_length=50, db_column=b'cPhone', blank=True)),
                ('is_active', models.BooleanField(default=None, db_column=b'nActive')),
                ('email_id', models.EmailField(max_length=75, db_column=b'cEmailId')),
                ('adminuser', models.BooleanField(default=None, db_column=b'nAdminUser')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, null=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac', blank=True)),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, db_column=b'cDataModiMac', blank=True)),
                ('branches', models.ManyToManyField(related_name='branch_relation', to='administration.BranchMaster')),
                ('company_id', models.ForeignKey(db_column=b'nCompanyID', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster')),
                ('data_add_by', models.ForeignKey(related_name='user_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True)),
                ('data_modi_by', models.ForeignKey(related_name='user_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('default_branch_id', models.ForeignKey(db_column=b'nDefaultBranchID', on_delete=django.db.models.deletion.PROTECT, to='administration.BranchMaster', null=True)),
                ('default_module_id', models.ForeignKey(db_column=b'nDefaultModuleID', on_delete=django.db.models.deletion.PROTECT, to='administration.ModuleMaster', null=True)),
                ('modules', models.ManyToManyField(related_name='module_relation', to='administration.ModuleMaster')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'tsmusermaster',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserRights',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('add_permitted', models.BooleanField(default=None, db_column=b'cAddPermitted')),
                ('edit_permitted', models.BooleanField(default=None, db_column=b'cEditPermitted')),
                ('delete_permitted', models.BooleanField(default=None, db_column=b'cDeletePermitted')),
                ('view_permitted', models.BooleanField(default=None, db_column=b'cViewPermitted')),
                ('data_add_on', models.DateTimeField(auto_now_add=True, db_column=b'dDataAddOn')),
                ('data_add_mac', models.CharField(max_length=50, db_column=b'cDataAddMac')),
                ('data_modi_on', models.DateTimeField(auto_now=True, null=True, db_column=b'dDataModiOn')),
                ('data_modi_mac', models.CharField(max_length=50, null=True, db_column=b'cDataModiMac', blank=True)),
                ('data_add_by', models.ForeignKey(related_name='usr_rights_creater', db_column=b'nDataAddBy', on_delete=django.db.models.deletion.PROTECT, to='administration.UserMaster')),
                ('data_modi_by', models.ForeignKey(related_name='usr_rights_updater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True)),
                ('menu_id', models.ForeignKey(related_name='menu_usr_rights_relation', db_column=b'nMenuID', to='administration.MenuDetails')),
                ('user_id', models.ForeignKey(related_name='user_user_rights_relation', db_column=b'nUserID', to='administration.UserMaster')),
            ],
            options={
                'db_table': 'tsmuserrights',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='menudetails',
            name='module_id',
            field=models.ForeignKey(to='administration.ModuleMaster', db_column=b'nModuleID'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='financialyear',
            name='data_add_by',
            field=models.ForeignKey(related_name='fin_year_creater', db_column=b'nDataAddBy', on_delete=django.db.models.deletion.PROTECT, to='administration.UserMaster'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='financialyear',
            name='data_modi_by',
            field=models.ForeignKey(related_name='fin_year_updater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='companymaster',
            name='data_add_by',
            field=models.ForeignKey(related_name='company_creator', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='companymaster',
            name='data_modi_by',
            field=models.ForeignKey(related_name='company_modifier', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='branchmaster',
            name='company_id',
            field=models.ForeignKey(related_name='branch_company_relation', db_column=b'nCompanyId', on_delete=django.db.models.deletion.PROTECT, to='administration.CompanyMaster'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='branchmaster',
            name='data_add_by',
            field=models.ForeignKey(related_name='branch_creater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataAddBy', blank=True, to='administration.UserMaster', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='branchmaster',
            name='data_modi_by',
            field=models.ForeignKey(related_name='branch_updater', on_delete=django.db.models.deletion.PROTECT, db_column=b'nDataModiBy', blank=True, to='administration.UserMaster', null=True),
            preserve_default=True,
        ),
    ]
