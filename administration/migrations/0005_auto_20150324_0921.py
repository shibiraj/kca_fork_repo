# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0004_auto_20150324_0917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menudetails',
            name='icon_class',
            field=models.CharField(max_length=50, null=True, db_column=b'cIconClass'),
            preserve_default=True,
        ),
    ]
