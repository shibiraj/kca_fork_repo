# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menudetails',
            name='parent_id',
            field=models.ForeignKey(db_column=b'nParentId', blank=True, to='administration.MenuDetails', null=True),
            preserve_default=True,
        ),
    ]
